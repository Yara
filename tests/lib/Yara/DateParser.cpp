/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "DateParser.h"
#include <Yara/Details/DateParser.h>

CPPUNIT_TEST_SUITE_REGISTRATION(DateParser);

void DateParser::setUp()
{}

void DateParser::tearDown()
{}

void DateParser::tryValidDate()
{
	Yara::Details::DateParser::parse("Sun, 11 Mar 2007 19:59 EDT");
}

