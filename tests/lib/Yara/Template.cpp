/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Template.h"
#include <Yara/Template.h>
#include <boost/filesystem/convenience.hpp>

CPPUNIT_TEST_SUITE_REGISTRATION(Template);

void Template::setUp()
{}

void Template::tearDown()
{}

void Template::tryCompileTemplate01()
{
	// templates don't necessarily have to contain any tags or whatever, so we can just compile anything into a template
	static const std::string header__("Header");
	static const std::string footer__("Footer");
	static const std::string item__("Item");
	Yara::Template::compile(boost::filesystem::current_path() / "test.rsstempl", header__, footer__, item__);
	// now read back and compare
	Yara::Template templ(boost::filesystem::current_path() / "test.rsstempl");
	CPPUNIT_ASSERT(header__ == templ.getHeader());
	CPPUNIT_ASSERT(footer__ == templ.getFooter());
	CPPUNIT_ASSERT(item__ == templ.getItem());
}
