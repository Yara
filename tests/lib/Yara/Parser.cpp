/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Parser.h"
#include <Yara/Parser.h>

CPPUNIT_TEST_SUITE_REGISTRATION(Parser);

void Parser::setUp()
{}

void Parser::tearDown()
{}

void Parser::tryCreateInstance()
{
	Yara::Parser parser;
	CPPUNIT_ASSERT_MESSAGE("A freshly created instance should resolve to true", parser);
}

void Parser::tryCompleteExample091()
{
	static const char * example =
		"<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>"
		"<rss version=\"0.91\">"
		"<channel>"
		"<title>WriteTheWeb</title>"
		"<link>http://writetheweb.com</link>"
		"<description>News for web users that write back</description>"
		"<language>en-us</language>"
		"<copyright>Copyright 2000, WriteTheWeb team.</copyright>"
		"<managingEditor>editor@writetheweb.com</managingEditor>"
		"<webMaster>webmaster@writetheweb.com</webMaster>"
		"<image>"
		"<title>WriteTheWeb</title>"
		"<url>http://writetheweb.com/images/mynetscape88.gif</url>"
		"<link>http://writetheweb.com</link>"
		"<width>88</width>"
		"<height>31</height>"
		"<description>News for web users that write back</description>"
		"</image>"
		"<item>"
		"<title>Giving the world a pluggable Gnutella</title>"
		"<link>http://writetheweb.com/read.php?item=24</link>"
		"<description>WorldOS is a framework on which to build programs that work like Freenet or Gnutella -allowing distributed applications using peer-to-peer routing.</description>"
		"</item>"
		"<item>"
		"<title>Syndication discussions hot up</title>"
		"<link>http://writetheweb.com/read.php?item=23</link>"
		"<description>After a period of dormancy, the Syndication mailing list has become active again, with contributions from leaders in traditional media and Web syndication.</description>"
		"</item>"
		"<item>"
		"<title>Personal web server integrates file sharing and messaging</title>"
		"<link>http://writetheweb.com/read.php?item=22</link>"
		"<description>The Magi Project is an innovative project to create a combined personal web server and messaging system that enables the sharing and synchronization of information across desktop, laptop and palmtop devices.</description>"
		"</item>"
		"<item>"
		"<title>Syndication and Metadata</title>"
		"<link>http://writetheweb.com/read.php?item=21</link>"
		"<description>RSS is probably the best known metadata format around. RDF is probably one of the least understood. In this essay, published on my O'Reilly Network weblog, I argue that the next generation of RSS should be based on RDF.</description>"
		"</item>"
		"<item>"
		"<title>UK bloggers get organised</title>"
		"<link>http://writetheweb.com/read.php?item=20</link>"
		"<description>Looks like the weblogs scene is gathering pace beyond the shores of the US. There's now a UK-specific page on weblogs.com, and a mailing list at egroups.</description>"
		"</item>"
		"<item>"
		"<title>Yournamehere.com more important than anything</title>"
		"<link>http://writetheweb.com/read.php?item=19</link>"
		"<description>Whatever you're publishing on the web, your site name is the most valuable asset you have, according to Carl Steadman.</description>"
		"</item>"
		"</channel>"
		"</rss>"
		;
	Yara::Parser parser;
	parser.parse(example);
	CPPUNIT_ASSERT_MESSAGE("The example feed should be completely parsed", parser.finish());
	CPPUNIT_ASSERT_MESSAGE("After a complete parse, the parser should not be able to parse again without being reset", !parser);

	CPPUNIT_ASSERT_MESSAGE("The RSS version should be 0.91", parser.getRSSVersion() == "0.91");
	CPPUNIT_ASSERT_MESSAGE("The channel title should have been extracted correctly", parser.getChannelInfo().title_ == "WriteTheWeb");
	CPPUNIT_ASSERT_MESSAGE("The channel link should have been extracted correctly", parser.getChannelInfo().link_ == "http://writetheweb.com");
	CPPUNIT_ASSERT_MESSAGE("The channel description should have been extracted correctly", parser.getChannelInfo().description_ == "News for web users that write back");
	CPPUNIT_ASSERT_MESSAGE("This channel is in english, as in from the US", parser.getChannelInfo().language_ == Yara::Details::english_united_states__);
	CPPUNIT_ASSERT_MESSAGE("The channel copyright should have been extracted correctly", parser.getChannelInfo().copyright_ == "Copyright 2000, WriteTheWeb team.");
	CPPUNIT_ASSERT_MESSAGE("The channel managing editor should have been extracted correctly", parser.getChannelInfo().managing_editor_ == "editor@writetheweb.com");
	CPPUNIT_ASSERT_MESSAGE("The channel webmaster should have been extracted correctly", parser.getChannelInfo().webmaster_ == "webmaster@writetheweb.com");

	CPPUNIT_ASSERT_MESSAGE("The channel image's title should have been extracted correctly", parser.getChannelInfo().image_->title_ == "WriteTheWeb");
	CPPUNIT_ASSERT_MESSAGE("The channel image's url should have been extracted correctly", parser.getChannelInfo().image_->url_ == "http://writetheweb.com/images/mynetscape88.gif");
	CPPUNIT_ASSERT_MESSAGE("The channel image's link should have been extracted correctly", parser.getChannelInfo().image_->link_ == "http://writetheweb.com");
	CPPUNIT_ASSERT_MESSAGE("The channel image's width should have been extracted correctly", parser.getChannelInfo().image_->width_ == 88);
	CPPUNIT_ASSERT_MESSAGE("The channel image's height should have been extracted correctly", parser.getChannelInfo().image_->height_ == 31);
	CPPUNIT_ASSERT_MESSAGE("The channel image's description should have been extracted correctly", parser.getChannelInfo().image_->description_ == "News for web users that write back");

	CPPUNIT_ASSERT_MESSAGE("There are six items in the stream - the parser should be able to count them", parser.getItemCount() == 6);

	static const char * item_titles__[] = {
		"Giving the world a pluggable Gnutella",
		"Syndication discussions hot up",
		"Personal web server integrates file sharing and messaging",
		"Syndication and Metadata",
		"UK bloggers get organised",
		"Yournamehere.com more important than anything"
	};
	static const char * item_links__[] = {
		"http://writetheweb.com/read.php?item=24",
		"http://writetheweb.com/read.php?item=23",
		"http://writetheweb.com/read.php?item=22",
		"http://writetheweb.com/read.php?item=21",
		"http://writetheweb.com/read.php?item=20",
		"http://writetheweb.com/read.php?item=19",
	};
	static const char * item_descriptions__[] = {
		"WorldOS is a framework on which to build programs that work like Freenet or Gnutella -allowing distributed applications using peer-to-peer routing.",
		"After a period of dormancy, the Syndication mailing list has become active again, with contributions from leaders in traditional media and Web syndication.",
		"The Magi Project is an innovative project to create a combined personal web server and messaging system that enables the sharing and synchronization of information across desktop, laptop and palmtop devices.",
		"RSS is probably the best known metadata format around. RDF is probably one of the least understood. In this essay, published on my O'Reilly Network weblog, I argue that the next generation of RSS should be based on RDF.",
		"Looks like the weblogs scene is gathering pace beyond the shores of the US. There's now a UK-specific page on weblogs.com, and a mailing list at egroups.",
		"Whatever you're publishing on the web, your site name is the most valuable asset you have, according to Carl Steadman."
	};
	for (unsigned int i(0); i < 6; ++i)
	{
		Yara::Details::ItemInfo item(parser.getItem(i));
		CPPUNIT_ASSERT_MESSAGE("The title should have been correct", item.title_ == item_titles__[i]);
		CPPUNIT_ASSERT_MESSAGE("The link should have been correct", item.link_ == item_links__[i]);
		CPPUNIT_ASSERT_MESSAGE("The description should have been correct", item.description_ == item_descriptions__[i]);
	}
}

void Parser::tryIncompleteExample091()
{
	static const char * example =
		"<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>"
		"<rss version=\"0.91\">"
		"<channel>"
		"<title>WriteTheWeb</title>"
		"<link>http://writetheweb.com</link>"
		"<description>News for web users that write back</description>"
		"<language>en-us</language>"
		"<copyright>Copyright 2000, WriteTheWeb team.</copyright>"
		"<managingEditor>editor@writetheweb.com</managingEditor>"
		"<webMaster>webmaster@writetheweb.com</webMaster>"
		"<image>"
		"<title>WriteTheWeb</title>"
		"<url>http://writetheweb.com/images/mynetscape88.gif</url>"
		"<link>http://writetheweb.com</link>"
		"<width>88</width>"
		;
	Yara::Parser parser;
	parser.parse(example);
	CPPUNIT_ASSERT_MESSAGE("After an incomplete parse, the parser should be able to parse again without being reset", parser);
	CPPUNIT_ASSERT_MESSAGE("The example feed should not be completely parsed", !parser.finish());
	CPPUNIT_ASSERT_MESSAGE("Once finish has been called, the parser is unable to parse", !parser);
}

void Parser::tryCompleteExample20()
{
	static const char * example = 
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		"<?xml-stylesheet href=\"http://rss.csmonitor.com/~d/styles/rss2full.xsl\" type=\"text/xsl\" media=\"screen\"?>"
		"<?xml-stylesheet href=\"http://rss.csmonitor.com/~d/styles/itemcontent.css\" type=\"text/css\" media=\"screen\"?>"
		"<rss xmlns:feedburner=\"http://rssnamespace.org/feedburner/ext/1.0\" version=\"2.0\">"
		"<channel>"
		"<title>Christian Science Monitor | Top Stories</title>"
		"<link>http://csmonitor.com</link>"
		"<description>Read the front page stories of csmonitor.com.</description>"
		"<language>en-us</language>"
		"<image>"
		"<link>http://csmonitor.com</link>"
		"<url>http://www.csmonitor.com/images/logo_88x31.gif</url>"
		"<title>Christian Science Monitor</title>"
		"<width>88</width>"
		"<height>31</height>"
		"</image>"
		"<copyright>Copyright 2007 The Christian Science Monitor. All rights reserved. This RSS file is offered to individuals and non-commercial organizations only. Newspapers, magazines, and other commercial websites wishing to use Monitor RSS files, please contact syndication@csmonitor.com.</copyright>"
		"<managingEditor>feedback@csmonitor.com</managingEditor>"
		"<atom10:link xmlns:atom10=\"http://www.w3.org/2005/Atom\" rel=\"self\" href=\"http://www.csmonitor.com/rss/top.rss\" type=\"application/rss+xml\" />"
		"<feedburner:browserFriendly>This is a feed from The Christian Science Monitor that allows you to get automatic updates on the news published at csmonitor.com. There's a list of all RSS feeds from the Monitor at csmonitor.com/rss</feedburner:browserFriendly>"
		"<item>"
		"<title>Capitol Hill closes in on immigration reform</title>"
		"<link>http://rss.csmonitor.com/~r/feeds/top/~3/101217361/p01s01-uspo.html</link>"
		"<description>Proposed bills would create a guest-worker program and a path to citizenship for illegal migrants.&lt;p&gt;&lt;a href=\"http://rss.csmonitor.com/~a/feeds/top?a=4peT60\"&gt;&lt;img src=\"http://rss.csmonitor.com/~a/feeds/top?i=4peT60\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/p&gt;&lt;div class=\"feedflare\"&gt;&lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=arqo7wXP\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=arqo7wXP\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=VvjFP5go\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=VvjFP5go\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=fqSwzi1L\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=fqSwzi1L\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=4vTbeP8c\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=4vTbeP8c\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/div&gt;&lt;img src=\"http://rss.csmonitor.com/~r/feeds/top/~4/101217361\"/&gt;</description>"
		"<feedburner:awareness>http://api.feedburner.com/awareness/1.0/GetItemData?uri=feeds/top&amp;itemurl=http%3A%2F%2Fwww.csmonitor.com%2F2007%2F0313%2Fp01s01-uspo.html</feedburner:awareness>"
		"<feedburner:origLink>http://www.csmonitor.com/2007/0313/p01s01-uspo.html</feedburner:origLink>"
		"</item>"
		"<item>"
		"<title>GOP voter discontent allows new entrants for '08</title>"
		"<link>http://rss.csmonitor.com/~r/feeds/top/~3/101217362/p01s02-uspo.html</link>"
		"<description>The Republican field swells to 13 presidential candidates, official and potential.&lt;p&gt;&lt;a href=\"http://rss.csmonitor.com/~a/feeds/top?a=LhFdX8\"&gt;&lt;img src=\"http://rss.csmonitor.com/~a/feeds/top?i=LhFdX8\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/p&gt;&lt;div class=\"feedflare\"&gt;&lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=wTbG56kp\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=wTbG56kp\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=maEb0ABD\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=maEb0ABD\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=yT6kprJP\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=yT6kprJP\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=dw0gdZtT\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=dw0gdZtT\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/div&gt;&lt;img src=\"http://rss.csmonitor.com/~r/feeds/top/~4/101217362\"/&gt;</description>"
		"<feedburner:awareness>http://api.feedburner.com/awareness/1.0/GetItemData?uri=feeds/top&amp;itemurl=http%3A%2F%2Fwww.csmonitor.com%2F2007%2F0313%2Fp01s02-uspo.html</feedburner:awareness>"
		"<feedburner:origLink>http://www.csmonitor.com/2007/0313/p01s02-uspo.html</feedburner:origLink>"
		"</item>"
		"<item>"
		"<title>'Fair trade' food booming in Britain</title>"
		"<link>http://rss.csmonitor.com/~r/feeds/top/~3/101217363/p01s03-woeu.html</link>"
		"<description>'Ethical eating,' a practice once restricted to the rich, is going mainstream.&lt;p&gt;&lt;a href=\"http://rss.csmonitor.com/~a/feeds/top?a=gbij9D\"&gt;&lt;img src=\"http://rss.csmonitor.com/~a/feeds/top?i=gbij9D\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/p&gt;&lt;div class=\"feedflare\"&gt;&lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=3BW1vtnR\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=3BW1vtnR\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=duCYiZWC\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=duCYiZWC\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=DEGgQohz\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=DEGgQohz\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=8nLmHvL2\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=8nLmHvL2\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/div&gt;&lt;img src=\"http://rss.csmonitor.com/~r/feeds/top/~4/101217363\"/&gt;</description>"
		"<feedburner:awareness>http://api.feedburner.com/awareness/1.0/GetItemData?uri=feeds/top&amp;itemurl=http%3A%2F%2Fwww.csmonitor.com%2F2007%2F0313%2Fp01s03-woeu.html</feedburner:awareness>"
		"<feedburner:origLink>http://www.csmonitor.com/2007/0313/p01s03-woeu.html</feedburner:origLink>"
		"</item>"
		"<item>"
		"<title>States scrutinize minors' security on MySpace</title>"
		"<link>http://rss.csmonitor.com/~r/feeds/top/~3/101217364/p03s03-stct.html</link>"
		"<description>Connecticut could become the first state to require social networking sites and chatrooms to verify a user's age.&lt;p&gt;&lt;a href=\"http://rss.csmonitor.com/~a/feeds/top?a=rSrQN6\"&gt;&lt;img src=\"http://rss.csmonitor.com/~a/feeds/top?i=rSrQN6\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/p&gt;&lt;div class=\"feedflare\"&gt;&lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=G56DDqUX\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=G56DDqUX\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=xE27ktX4\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=xE27ktX4\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=IhduDlpP\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=IhduDlpP\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=KkGs7qlx\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=KkGs7qlx\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/div&gt;&lt;img src=\"http://rss.csmonitor.com/~r/feeds/top/~4/101217364\"/&gt;</description>"
		"<feedburner:awareness>http://api.feedburner.com/awareness/1.0/GetItemData?uri=feeds/top&amp;itemurl=http%3A%2F%2Fwww.csmonitor.com%2F2007%2F0313%2Fp03s03-stct.html</feedburner:awareness>"
		"<feedburner:origLink>http://www.csmonitor.com/2007/0313/p03s03-stct.html</feedburner:origLink>"
		"</item>"
		"<item>"
		"<title>The monied heroes of a new Vietnam</title>"
		"<link>http://rss.csmonitor.com/~r/feeds/top/~3/101217365/p06s01-woap.html</link>"
		"<description>After a decade of roaring growth, wealth is no longer something to hide in the once unwaveringly Communist nation.&lt;p&gt;&lt;a href=\"http://rss.csmonitor.com/~a/feeds/top?a=bG1y6N\"&gt;&lt;img src=\"http://rss.csmonitor.com/~a/feeds/top?i=bG1y6N\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/p&gt;&lt;div class=\"feedflare\"&gt;&lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=32lHDtsw\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=32lHDtsw\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=RVXzNhfv\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=RVXzNhfv\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=JmGTM2QI\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=JmGTM2QI\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt; &lt;a href=\"http://rss.csmonitor.com/~f/feeds/top?a=DgAzSQ5N\"&gt;&lt;img src=\"http://rss.csmonitor.com/~f/feeds/top?i=DgAzSQ5N\" border=\"0\"&gt;&lt;/img&gt;&lt;/a&gt;&lt;/div&gt;&lt;img src=\"http://rss.csmonitor.com/~r/feeds/top/~4/101217365\"/&gt;</description>"
		"<feedburner:awareness>http://api.feedburner.com/awareness/1.0/GetItemData?uri=feeds/top&amp;itemurl=http%3A%2F%2Fwww.csmonitor.com%2F2007%2F0313%2Fp06s01-woap.html</feedburner:awareness>"
		"<feedburner:origLink>http://www.csmonitor.com/2007/0313/p06s01-woap.html</feedburner:origLink>"
		"</item>"
		"<feedburner:awareness>http://api.feedburner.com/awareness/1.0/GetFeedData?uri=feeds/top</feedburner:awareness>"
		"</channel>"
		"</rss>"
		;
	Yara::Parser parser;
	parser.parse(example);
	CPPUNIT_ASSERT_MESSAGE("The example feed should be completely parsed", parser.finish());
	CPPUNIT_ASSERT_MESSAGE("After a complete parse, the parser should not be able to parse again without being reset", !parser);
}