/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_tests_parser_h
#define _yara_tests_parser_h

#include <cppunit/extensions/HelperMacros.h>

class Parser : public CPPUNIT_NS::TestFixture
{
	// Tests registration
	CPPUNIT_TEST_SUITE( Parser );
	CPPUNIT_TEST( tryCreateInstance );
	CPPUNIT_TEST( tryCompleteExample091 );
	CPPUNIT_TEST( tryIncompleteExample091 );
	CPPUNIT_TEST( tryCompleteExample20 );
	CPPUNIT_TEST_SUITE_END();

public:
	virtual void setUp();
	virtual void tearDown();

protected:
	void tryCreateInstance();
	void tryCompleteExample091();
	void tryIncompleteExample091();
	void tryCompleteExample20();

private:
	// Fixture data
	// none for the moment
};

#endif