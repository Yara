/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_tests_parser_h
#define _yara_tests_parser_h

#include <cppunit/extensions/HelperMacros.h>

class Template : public CPPUNIT_NS::TestFixture
{
	// Tests registration
	CPPUNIT_TEST_SUITE( Template );
	CPPUNIT_TEST(tryCompileTemplate01);
	CPPUNIT_TEST_SUITE_END();

public:
	virtual void setUp();
	virtual void tearDown();

protected:
	void tryCompileTemplate01();

private:
	// Fixture data
	// none for the moment
};

#endif