Test_Yara_SRC :=			DateParser.cpp  main.cpp  Parser.cpp  Template.cpp Generator.cpp

SRC += $(patsubst %,test/lib/Yara/%,$(Test_Yara_SRC))

Test_Yara_OBJ := $(patsubst %.cpp,tests/lib/Yara/%.lo,$(Test_Yara_SRC))

OBJ += $(Test_Yara_OBJ)

TEST_LDFLAGS=libYara.la $(LIBS) -lcppunit -lltdl -lexpat -lboost_filesystem-gcc-mt-1_33_1
$(eval $(call LINK_BINARY_template,Test_Yara.bin,$(Test_Yara_OBJ),$(TEST_LDFLAGS)))
CHECK_DEPS += Test_Yara.bin 


