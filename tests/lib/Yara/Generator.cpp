/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Generator.h"
#include <Yara/Generator.h>
#include <Yara/Exceptions/GeneratorExceptions.h>
#include <Yara/Details/ImageInfo.h>

CPPUNIT_TEST_SUITE_REGISTRATION(Generator);

void Generator::setUp()
{}

void Generator::tearDown()
{}

void Generator::tryGenerateStream01()
{
	Yara::Generator generator;
	CPPUNIT_ASSERT_THROW(generator.generate(), Yara::Exceptions::ChannelInfoInvalid);
}

void Generator::tryGenerateStream02()
{
	Yara::Generator generator;
	Yara::Details::ChannelInfo channel_info;
	channel_info.title_ = "The Developer's Corner";
	channel_info.link_ = "https://vlinder.landheer-cieslak.com";
	channel_info.description_ = "A site for newbies to be trained and an incubator for projects in C++";
	channel_info.language_ = Yara::Details::english__;
	channel_info.copyright_ = "Ronald Landheer-Cieslak";
	channel_info.managing_editor_ = "Ronald Landheer-Cieslak";
	channel_info.webmaster_ = "Ronald Landheer-Cieslak";
	generator.setChannelInfo(channel_info);
	std::string generated = std::string(generator.generate());
	CPPUNIT_ASSERT(generated == 
		"<?xml version=\"1.0\" ?>\n"
		"<rss version=\"2.0\">\n"
		"\t<channel>\n"
		"\t\t<title>The Developer's Corner</title>\n"
		"\t\t<link>https://vlinder.landheer-cieslak.com</link>\n"
		"\t\t<description>A site for newbies to be trained and an incubator for projects in C++</description>\n"
		"\t\t<language>en</language>\n"
		"\t\t<copyright>Ronald Landheer-Cieslak</copyright>\n"
		"\t\t<managingEditor>Ronald Landheer-Cieslak</managingEditor>\n"
		"\t\t<webMaster>Ronald Landheer-Cieslak</webMaster>\n"
		"\t</channel>\n"
		"</rss>\n"
		);
}

void Generator::tryGenerateStream03()
{
	Yara::Generator generator;
	Yara::Details::ChannelInfo channel_info;
	channel_info.title_ = "The Developer's Corner";
	channel_info.link_ = "https://vlinder.landheer-cieslak.com";
	channel_info.description_ = "A site for newbies to be trained and an incubator for projects in C++";
	channel_info.language_ = Yara::Details::english__;
	channel_info.copyright_ = "Ronald Landheer-Cieslak";
	channel_info.managing_editor_ = "Ronald Landheer-Cieslak";
	channel_info.webmaster_ = "Ronald Landheer-Cieslak";
	channel_info.image_.reset(new Yara::Details::ImageInfo);
	channel_info.image_->title_ = "logo";
	channel_info.image_->url_ = "https://vlinder.landheer-cieslak.com/~ronald/cpp/vlinder_135.png";
	channel_info.image_->link_ = "https://vlinder.landheer-cieslak.com";
	channel_info.image_->width_ = 135;
	channel_info.image_->height_ = 114;
	channel_info.image_->description_ = "A blue butterfly";
	generator.setChannelInfo(channel_info);
	std::string generated = std::string(generator.generate());
	CPPUNIT_ASSERT(generated == 
		"<?xml version=\"1.0\" ?>\n"
		"<rss version=\"2.0\">\n"
		"\t<channel>\n"
		"\t\t<title>The Developer's Corner</title>\n"
		"\t\t<link>https://vlinder.landheer-cieslak.com</link>\n"
		"\t\t<description>A site for newbies to be trained and an incubator for projects in C++</description>\n"
		"\t\t<language>en</language>\n"
		"\t\t<copyright>Ronald Landheer-Cieslak</copyright>\n"
		"\t\t<managingEditor>Ronald Landheer-Cieslak</managingEditor>\n"
		"\t\t<webMaster>Ronald Landheer-Cieslak</webMaster>\n"
		"\t\t<image>\n"
		"\t\t\t<title>logo</title>\n"
		"\t\t\t<url>https://vlinder.landheer-cieslak.com/~ronald/cpp/vlinder_135.png</url>\n"
		"\t\t\t<link>https://vlinder.landheer-cieslak.com</link>\n"
		"\t\t\t<width>135</width>\n"
		"\t\t\t<height>114</height>\n"
		"\t\t\t<description>A blue butterfly</description>\n"
		"\t\t</image>\n"
		"\t</channel>\n"
		"</rss>\n"
		);
}

void Generator::tryGenerateStream04()
{
	Yara::Generator generator;
	Yara::Details::ChannelInfo channel_info;
	channel_info.title_ = "The Developer's Corner";
	channel_info.link_ = "https://vlinder.landheer-cieslak.com";
	channel_info.description_ = "A site for newbies to be trained and an incubator for projects in C++";
	channel_info.language_ = Yara::Details::english__;
	channel_info.copyright_ = "Ronald Landheer-Cieslak";
	channel_info.managing_editor_ = "Ronald Landheer-Cieslak";
	channel_info.webmaster_ = "Ronald Landheer-Cieslak";
	channel_info.image_.reset(new Yara::Details::ImageInfo);
	channel_info.image_->title_ = "logo";
	channel_info.image_->url_ = "https://vlinder.landheer-cieslak.com/~ronald/cpp/vlinder_135.png";
	channel_info.image_->link_ = "https://vlinder.landheer-cieslak.com";
	channel_info.image_->width_ = 135;
	channel_info.image_->height_ = 114;
	channel_info.image_->description_ = "A blue butterfly";
	generator.setChannelInfo(channel_info);
	generator.addItem(Yara::Details::ItemInfo("title", "url", "desc"));
	std::string generated = std::string(generator.generate());
	CPPUNIT_ASSERT(generated == 
		"<?xml version=\"1.0\" ?>\n"
		"<rss version=\"2.0\">\n"
		"\t<channel>\n"
		"\t\t<title>The Developer's Corner</title>\n"
		"\t\t<link>https://vlinder.landheer-cieslak.com</link>\n"
		"\t\t<description>A site for newbies to be trained and an incubator for projects in C++</description>\n"
		"\t\t<language>en</language>\n"
		"\t\t<copyright>Ronald Landheer-Cieslak</copyright>\n"
		"\t\t<managingEditor>Ronald Landheer-Cieslak</managingEditor>\n"
		"\t\t<webMaster>Ronald Landheer-Cieslak</webMaster>\n"
		"\t\t<image>\n"
		"\t\t\t<title>logo</title>\n"
		"\t\t\t<url>https://vlinder.landheer-cieslak.com/~ronald/cpp/vlinder_135.png</url>\n"
		"\t\t\t<link>https://vlinder.landheer-cieslak.com</link>\n"
		"\t\t\t<width>135</width>\n"
		"\t\t\t<height>114</height>\n"
		"\t\t\t<description>A blue butterfly</description>\n"
		"\t\t</image>\n"
		"\t\t<item>\n"
		"\t\t\t<title>title</title>\n"
		"\t\t\t<link>url</link>\n"
		"\t\t\t<description>desc</description>\n"
		"\t\t</item>\n"
		"\t</channel>\n"
		"</rss>\n"
		);
}
