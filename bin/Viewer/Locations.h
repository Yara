#ifndef _viewer_locations_h
#define _viewer_locations_h

#include <string>
#include <vector>

namespace Viewer
{
	class Locations
	{
	public :
		struct Location
		{
			Location(const std::string & title, const std::string & url)
				: title_(title),
				  url_(url)
			{ /* no-op */ }

			std::string title_;
			std::string url_;
		};

		static Locations & getInstance();

		void addLocation(const Location & location);
		std::string getDefaultLocation() const;
		std::vector< Location > getLocations() const;
		std::string getLocationAddress(const std::string & location_name) const;

	private :
		struct Data;

		// privately constructible only
		Locations();
		~Locations();

		// Neither CopyConstructible nor Assignable
		Locations(const Locations &);
		Locations & operator=(const Locations &);

		static std::vector< Location > fromBinaryData(const std::vector< char > & buffer);
		static std::vector< char > toBinaryData(const std::vector< Location > & strings);

		void open();

		Data * data_;
		static Locations * instance__;
	};
}

#endif

