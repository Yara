//---------------------------------------------------------------------------

#ifndef AddLocationFormH
#define AddLocationFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class Tadd_location__ : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TEdit *name_edit_;
	TLabel *Label2;
	TEdit *address_edit_;
	TButton *save_button_;
	TButton *cancel_button_;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall cancel_button_Click(TObject *Sender);
	void __fastcall save_button_Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall Tadd_location__(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tadd_location__ *add_location__;
//---------------------------------------------------------------------------
#endif
