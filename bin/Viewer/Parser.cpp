#include "Parser.h"
#include <memory>
#include <stdexcept>
#include <windows.h>
#include <vcl/Registry.hpp>

namespace Viewer
{
	struct Parser::Data
	{
		typedef int (*Renderer)(const char * /* target_filename */, const char * /*template_filename */, const char * /*rss_xml*/);
		Data()
			: parser_library_(NULL),
			  renderer_(NULL)
		{ /* no-op */ }

		~Data()
		{
			if (parser_library_ != NULL)
				FreeLibrary(parser_library_);
			else
			{ /* never opened */ }
		}

		HMODULE parser_library_;
		Renderer renderer_;
	};

	Parser * Parser::instance__(0);

	Parser & Parser::getInstance()
	{
		if (!instance__)
			instance__ = new Parser;
		else
		{ /* already have a parser */ }

		return *instance__;
	}

	void Parser::render(const std::string & target_filename, const std::string & template_filename, const std::string & rss_xml)
	{
		data_->renderer_(target_filename.c_str(), template_filename.c_str(), rss_xml.c_str());
	}


	Parser::Parser()
		: data_(new Data)
	{
		std::auto_ptr< TRegistry > registry(new TRegistry);
		// get the location of the parser DLL from the registry
		if (!registry->OpenKey("Software\\Ronald Landheer-Cieslak\\YaraViewer", false))
			throw std::runtime_error("Failed to open configuration registry key");
		else
		{ /* all is well */ }
		std::string library_path(registry->ReadString("ParserLocation").c_str());
		data_->parser_library_ = LoadLibrary(library_path.c_str());
		if (data_->parser_library_ == NULL)
			throw std::runtime_error("Failed to load parser library");
		else
		{ /* parser library loaded */ }
		data_->renderer_ = (Data::Renderer)GetProcAddress(data_->parser_library_, "Yara_render");
		if (!data_->renderer_)
			throw std::runtime_error("Failed to find rendering function");
		else
		{ /* all is well */ }
	}

	Parser::~Parser()
	{
		delete data_;
	}
}
