#include "Locations.h"
#include <algorithm>
#include <iterator>
#include <vcl/Registry.hpp>
#include "ConfirmationDialog.h"

namespace Viewer
{
	namespace
	{
		struct LocationFinder
		{
			enum What { find_by_name__, find_by_address__ };

			LocationFinder(What what, const std::string & value)
				: what_(what),
				  value_(value)
			{ /* no-op */ }

			bool operator()(const Locations::Location & location)
			{
				switch (what_)
				{
				case find_by_name__ :
					return location.title_ == value_;
				case find_by_address__ :
					return location.url_ == value_;
				default:
					throw std::logic_error("Don't know what to look for!");
				}
			}

			What what_;
			std::string value_;
		};
	}
	struct Locations::Data
	{
		Data()
			: registry_(new TRegistry)
		{ /* no-op */ }

		~Data()
		{
			delete registry_;
		}

		TRegistry * registry_;
	};
	Locations * Locations::instance__(0);

	Locations & Locations::getInstance()
	{
		if (!instance__)
			instance__ = new Locations;
		else
		{ /* already have an instance */ }

		return *instance__;
	}

	void Locations::addLocation(const Location & location)
	{
		bool add_location(true);
		std::vector< Location > locations(getLocations());
		std::vector< Location >::iterator where;

		if ((where = std::find_if(locations.begin(), locations.end(), LocationFinder(LocationFinder::find_by_name__, location.title_))) != locations.end())
		{
			confirmation_dialog__->text_->Caption =
				"A location with the given name already exists.\n"
				"Would you like to set the address you've set\n"
				"for this entry to that one (Modify),\n"
				"or cancel adding the location (Cancel)?";
			confirmation_dialog__->text_->Width = confirmation_dialog__->bevel_->Width - 16;
			confirmation_dialog__->text_->Height = confirmation_dialog__->bevel_->Height - 16;
			confirmation_dialog__->add_anyway_button_->Enabled = false;
			confirmation_dialog__->modify_button_->Caption = "Modify";
			int confirmation_result(confirmation_dialog__->ShowModal());
			switch (confirmation_result)
			{
			case mrRetry : // modify existing
				add_location = false;
				*where = location;
				break;
			case mrCancel : // cancel addition
				return ;
			default:
				throw std::logic_error("Don't know what to do!");
			}
		}
		else
		{ /* not a duplicate */ }
		if (add_location && (where = std::find_if(locations.begin(), locations.end(), LocationFinder(LocationFinder::find_by_address__, location.url_))) != locations.end())
		{
			confirmation_dialog__->text_->Caption =
				"A location with the given address already exists.\n"
				"Would you like to rename the entry with the given\n"
				"address (Rename), add a location with a duplicate\n"
				"name (Add anyway) or cancel adding the location (Cancel)?";
			confirmation_dialog__->text_->Width = confirmation_dialog__->bevel_->Width - 16;
			confirmation_dialog__->text_->Height = confirmation_dialog__->bevel_->Height - 16;
			confirmation_dialog__->modify_button_->Caption = "Rename";
			confirmation_dialog__->add_anyway_button_->Enabled = true;
			int confirmation_result(confirmation_dialog__->ShowModal());
			switch (confirmation_result)
			{
			case mrOk : // add anyway
				break;
			case mrRetry : // modify existing
				add_location = false;
				*where = location;
				break;
			case mrCancel : // cancel addition
				return ;
			default:
				throw std::logic_error("Don't know what to do!");
			}
		}
		else
		{ /* not a duplicate */ }

		if (add_location)
			locations.push_back(location);
		else
		{ /* in-place modification */ }
		std::vector< char > buffer(toBinaryData(locations));
		data_->registry_->WriteBinaryData("Locations", &(buffer[0]), static_cast<int>(buffer.size()));
		data_->registry_->WriteInteger("LocationsSize", static_cast<int>(buffer.size()));
	}

	std::string Locations::getDefaultLocation() const
	{
		std::string default_location(data_->registry_->ReadString("DefaultLocation").c_str());
		if (default_location.empty())
			default_location = "http://www.landheer-cieslak.com/index.php?option=com_rss&feed=RSS0.91&no_html=1";
		else
		{ /* we have a real default location, so we don't need the default default */ }

		return default_location;
	}

	std::vector< Locations::Location > Locations::getLocations() const
	{
		std::vector< Location > locations;

		if (data_->registry_->ValueExists("LocationsSize") && data_->registry_->ValueExists("Locations"))
		{
			// first, get the amount of data that should be there
			unsigned int data_size(data_->registry_->ReadInteger("LocationsSize"));
			// now, get all the data
			std::vector< char > buffer(data_size);
			if (static_cast<unsigned int>(data_->registry_->ReadBinaryData("Locations", &(buffer[0]), data_size)) != data_size)
				throw std::runtime_error("Failed to read locations from registry");
			else
			{ /* all is well so far */ }
			locations = fromBinaryData(buffer);
		}
		else
		{ /* nothing there yet */ }

		return locations;
	}

	std::string Locations::getLocationAddress(const std::string & location_name) const
	{
		std::vector< Location > locations(getLocations());
		std::vector< Location >::const_iterator where(std::find_if(locations.begin(), locations.end(), LocationFinder(LocationFinder::find_by_name__, location_name)));
		if (where == locations.end())
			throw std::runtime_error("Location with the given name not found");
		else
		{ /* location found */ }
		return where->url_;
	}

	Locations::Locations()
		: data_(new Data)
	{
		open();
	}

	Locations::~Locations()
	{
		delete data_;
	}

	std::vector< Locations::Location > Locations::fromBinaryData(const std::vector< char > & buffer)
	{
		std::vector< std::string > temp;
		{ // extract the strings from the registry
		std::vector< char >::const_iterator where(buffer.begin());
		std::vector< char >::const_iterator end(buffer.end());
		while (where != end)
		{
			std::vector< char >::const_iterator whence(std::find(where, end, 0));
			if (where != whence)
				temp.push_back(std::string(where, whence));
			else
			{ /* won't add an empty string */ }
			where = whence;
			if (where != end)
				where++;
			else
			{ /* already at end */ }
		}
		} // done extracting strings from the registry - they are now in temp

		if (temp.size() % 2 != 0)
			throw std::runtime_error("Invalid entries in registry");
		else
		{ /* we have an even amount of strings */ }

		std::vector< Location > retval;
		{ // extract the locations from the strings
		std::vector< std::string >::const_iterator where(temp.begin());
		std::vector< std::string >::const_iterator end(temp.end());
		while (where != end)
		{
			assert(std::distance(where, end) >= 2);
			retval.push_back(Location(*where, *(where + 1)));
			std::advance(where, 2);
		}
		} // done extracting. Locations are now in retval

		return retval;
	}

	std::vector< char > Locations::toBinaryData(const std::vector< Locations::Location > & locations)
	{
		std::vector< char > retval;

		std::vector< Locations::Location >::const_iterator where(locations.begin());
		std::vector< Locations::Location >::const_iterator end(locations.end());
		for ( ; where != end; ++where)
		{
			std::copy(where->title_.begin(), where->title_.end(), std::back_inserter(retval));
			retval.push_back(0);
			std::copy(where->url_.begin(), where->url_.end(), std::back_inserter(retval));
			retval.push_back(0);
		}
		retval.push_back(0);

		return retval;
	}

	void Locations::open()
	{
		if (data_->registry_->CurrentPath != "Software\\Ronald Landheer-Cieslak\\YaraViewer")
		{
			if (!data_->registry_->OpenKey("Software\\Ronald Landheer-Cieslak\\YaraViewer", false))
				throw std::runtime_error("Failed to open registry key");
			else
			{ /* key opened OK */ }
		}
		else
		{ /* key already open */ }
	}
}
