//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "AddLocationForm.h"
#include "MainForm.h"
#include "Locations.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tadd_location__ *add_location__;
//---------------------------------------------------------------------------
__fastcall Tadd_location__::Tadd_location__(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tadd_location__::FormShow(TObject *Sender)
{
	viewer__->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall Tadd_location__::FormClose(TObject *Sender,
	  TCloseAction &Action)
{
	viewer__->update();
	viewer__->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall Tadd_location__::cancel_button_Click(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------


void __fastcall Tadd_location__::save_button_Click(TObject *Sender)
{
	Viewer::Locations::getInstance().addLocation(Viewer::Locations::Location(name_edit_->Text.c_str(), address_edit_->Text.c_str()));
	Close();
}
//---------------------------------------------------------------------------

