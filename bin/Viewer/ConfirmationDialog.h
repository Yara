//----------------------------------------------------------------------------
#ifndef ConfirmationDialogH
#define ConfirmationDialogH
//----------------------------------------------------------------------------
#include <vcl\ExtCtrls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\Classes.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Windows.hpp>
#include <vcl\System.hpp>
//----------------------------------------------------------------------------
class Tconfirmation_dialog__ : public TForm
{
__published:
	TButton *add_anyway_button_;
	TButton *modify_button_;
	TBevel *bevel_;
	TStaticText *text_;
	TButton *cancel_button_;
private:
public:
	virtual __fastcall Tconfirmation_dialog__(TComponent* AOwner);
};
//----------------------------------------------------------------------------
extern PACKAGE Tconfirmation_dialog__ *confirmation_dialog__;
//----------------------------------------------------------------------------
#endif
