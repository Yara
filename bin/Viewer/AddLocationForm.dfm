object add_location__: Tadd_location__
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Add a stream location'
  ClientHeight = 93
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label2: TLabel
    Left = 8
    Top = 35
    Width = 39
    Height = 13
    Caption = 'Address'
  end
  object name_edit_: TEdit
    Left = 53
    Top = 5
    Width = 251
    Height = 21
    TabOrder = 0
  end
  object address_edit_: TEdit
    Left = 53
    Top = 32
    Width = 251
    Height = 21
    TabOrder = 1
  end
  object save_button_: TButton
    Left = 229
    Top = 59
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 2
    OnClick = save_button_Click
  end
  object cancel_button_: TButton
    Left = 148
    Top = 59
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 3
    OnClick = cancel_button_Click
  end
end
