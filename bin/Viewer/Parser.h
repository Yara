#ifndef _viewer_parser_h
#define _viewer_parser_h

#include <string>

namespace Viewer
{
	class Parser
	{
	public :
		static Parser & getInstance();

		void render(const std::string & target_filename, const std::string & template_filename, const std::string & rss_xml);
	private :
		struct Data;

		// Neither CopyConstructible nor Assignable
		Parser(const Parser &);
		Parser & operator=(const Parser &);

		// Privately constructible only
		Parser();
		~Parser();

		Data * data_;
		static Parser * instance__;
	};
}

#endif
