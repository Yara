object confirmation_dialog__: Tconfirmation_dialog__
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Confirm'
  ClientHeight = 179
  ClientWidth = 384
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object bevel_: TBevel
    Left = 8
    Top = 8
    Width = 281
    Height = 161
    Shape = bsFrame
  end
  object add_anyway_button_: TButton
    Left = 300
    Top = 7
    Width = 75
    Height = 25
    Caption = 'Add anyway'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object modify_button_: TButton
    Left = 300
    Top = 38
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Rename'
    ModalResult = 4
    TabOrder = 1
  end
  object text_: TStaticText
    Left = 16
    Top = 16
    Width = 30
    Height = 17
    Caption = 'text_'
    TabOrder = 2
  end
  object cancel_button_: TButton
    Left = 300
    Top = 69
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
