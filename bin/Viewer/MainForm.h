//---------------------------------------------------------------------------

#ifndef MainFormH
#define MainFormH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <OleCtrls.hpp>
#include <SHDocVw.hpp>
#include <ActnList.hpp>
#include <ExtActns.hpp>
#include <ComCtrls.hpp>
#include <string>
#include <vcl/ExtActns.hpp>

//---------------------------------------------------------------------------
class Tviewer__ : public TForm
{
__published:	// IDE-managed Components
	TMainMenu *main_menu_;
	TMenuItem *Streams1;
	TMenuItem *Addlocation1;
	TMenuItem *Refresh1;
	TWebBrowser *explorer_;
	TDownLoadURL * download_action_;
	TProgressBar *progress_bar_;
	TMenuItem *Locations1;

	void __fastcall FormResize(TObject *Sender);
	void __fastcall download_action_DownloadProgress(TDownLoadURL *Sender,
          DWORD Progress, DWORD ProgressMax, TURLDownloadStatus StatusCode,
          AnsiString StatusText, bool &Cancel);
	void __fastcall Addlocation1Click(TObject *Sender);
	void __fastcall Refresh1Click(TObject *Sender);
private:	// User declarations
	void loadLocation(const std::string & location);
	void fillLocationsMenu();

public:		// User declarations
	__fastcall Tviewer__(TComponent* Owner);
	void update();
	void __fastcall LocationClick(TObject *Sender);

	private :
	std::string current_location_;
};
//---------------------------------------------------------------------------
extern PACKAGE Tviewer__ *viewer__;
//---------------------------------------------------------------------------
#endif
