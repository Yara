//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MainForm.h"
#include <iterator>
#include "AddLocationForm.h"
#include "Locations.h"
#include "Parser.h"
#include <vcl/Registry.hpp>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tviewer__ *viewer__;
//---------------------------------------------------------------------------
__fastcall Tviewer__::Tviewer__(TComponent* Owner)
	: TForm(Owner),
	  download_action_(new TDownLoadURL(Owner))
{
	current_location_ = Viewer::Locations::getInstance().getDefaultLocation();
	update();
}
//---------------------------------------------------------------------------
void __fastcall Tviewer__::FormResize(TObject *Sender)
{
	explorer_->Height = ClientHeight - 32;
	explorer_->Width = ClientWidth;
	progress_bar_->Top = ClientHeight - 25;
}
//---------------------------------------------------------------------------

void Tviewer__::loadLocation(const std::string & location)
{
	current_location_ = location;
	progress_bar_->Min = 0;
	download_action_->Filename = "test.rss";
	download_action_->URL = location.c_str();
	download_action_->ExecuteTarget(0);
	std::auto_ptr< TFileStream > file(new TFileStream("test.rss", fmOpenRead));
	std::vector< char > buffer(4096); // a 4K buffer
	std::vector< char > content;
	int size_read(0);
	while ((size_read = file->Read(&(buffer[0]), 4096)) > 0)
		std::copy(buffer.begin(), buffer.begin() + size_read, std::back_inserter(content));

	std::auto_ptr< TRegistry > registry(new TRegistry);
	if (!registry->OpenKey("Software\\Ronald Landheer-Cieslak\\YaraViewer", false))
		throw std::runtime_error("Failed to open registry key");
	else
	{ /* key opened OK */ }

	char temp_path[MAX_PATH + 1];
	GetTempPathA(MAX_PATH, temp_path);
	Viewer::Parser::getInstance().render((AnsiString(temp_path) + "test.html").c_str(), registry->ReadString("TemplateLocation").c_str(), std::string(content.begin(), content.end()));
	explorer_->Navigate(AnsiString("file:///") + temp_path + "test.html");
}

void __fastcall Tviewer__::download_action_DownloadProgress(
	  TDownLoadURL *Sender, DWORD Progress, DWORD ProgressMax,
	  TURLDownloadStatus StatusCode, AnsiString StatusText, bool &Cancel)
{
	progress_bar_->Max = ProgressMax;
	progress_bar_->Position = Progress;
}
//---------------------------------------------------------------------------

void Tviewer__::fillLocationsMenu()
{
	std::vector< Viewer::Locations::Location > locations(Viewer::Locations::getInstance().getLocations());
	std::vector< Viewer::Locations::Location >::const_iterator where(locations.begin());
	std::vector< Viewer::Locations::Location >::const_iterator end(locations.end());
	if (where == end)
		Locations1->Enabled = false;
	else
	{
		Locations1->Enabled = true;
		Locations1->Clear();
		for (; where != end; ++where)
		{
			TMenuItem * new_menu_item(new TMenuItem(Locations1));
			new_menu_item->OnClick = &LocationClick;
			new_menu_item->Caption = where->title_.c_str();
			Locations1->Add(new_menu_item);
		}
	}
}
void __fastcall Tviewer__::Addlocation1Click(TObject *Sender)
{
	add_location__->name_edit_->Text = "";
	add_location__->address_edit_->Text = "";
	add_location__->Show();
	update();
}
//---------------------------------------------------------------------------

void __fastcall Tviewer__::LocationClick(TObject *Sender)
{
	TMenuItem * menu_item = (TMenuItem*)Sender;
	std::string name(menu_item->Caption.c_str());
	std::string::size_type offs(std::string::npos);
	while ((offs = name.find('&')) != std::string::npos)
		name.erase(offs, 1);
	loadLocation(Viewer::Locations::getInstance().getLocationAddress(name));
}
//---------------------------------------------------------------------------

void Tviewer__::update()
{
	fillLocationsMenu();
	loadLocation(current_location_);
}

void __fastcall Tviewer__::Refresh1Click(TObject *Sender)
{
	update();
}
//---------------------------------------------------------------------------

