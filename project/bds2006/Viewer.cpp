//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("..\..\bin\Viewer\MainForm.cpp", viewer__);
USEFORM("..\..\bin\Viewer\AddLocationForm.cpp", add_location__);
USEFORM("..\..\bin\Viewer\ConfirmationDialog.cpp", confirmation_dialog__);
//---------------------------------------------------------------------------

#include <stdexcept>
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		Application->Title = "Yara Viewer";
		Application->CreateForm(__classid(Tviewer__), &viewer__);
		Application->CreateForm(__classid(Tadd_location__), &add_location__);
		Application->CreateForm(__classid(Tconfirmation_dialog__), &confirmation_dialog__);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (const std::exception & exception)
	{
		try
		{
			throw Exception(exception.what());
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	catch (...)
	{
		try
		{
			throw Exception("Unknown error");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
