/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_template_h
#define _yara_template_h
/** \file Yara/Template.h Defines the Template class.
 * Part of the C++ API for Yara, defines the class that loads and manages
 * the templates used for rendering. */
#ifdef DOXYGEN_GENERATING
#include <Yara/Template.h>
#else
#include "Details/prologue.h"
#include <string>

namespace boost
{
	namespace filesystem
	{
		class path;
	}
}
#endif
namespace Yara
{
	/** \brief Class to manage rendering templates.
	 * Yara uses templates to render RSS streams. This class loads the template
	 * and provides access to it. It can also compile a template from the contents
	 * of three strings. */
	class Yara_API Template
	{
	public :
		//! Default constructor. Creates an empty template.
		Template();
		//! Load the given template from disk.
		Template(const boost::filesystem::path & filename);
		//! Copy constructor. Implements value-type semantics.
		Template(const Template & rhs)
			: header_(rhs.header_),
			  footer_(rhs.footer_),
			  item_(rhs.item_)
		{ /* no-op */ }
		~Template();

		//! Assignment operator. Implements value-type semantics.
		Template & operator=(const Template & rhs)
		{
			header_ = rhs.header_;
			footer_ = rhs.footer_;
			item_ = rhs.item_;

			return *this;
		}

		//! Get the header template - may be empty.
		std::string getHeader() const { return header_; }
		//! Get the footer template - may be empty.
		std::string getFooter() const { return footer_; }
		//! Get the item template - may be empty.
		std::string getItem() const { return item_; }

		/** \brief Compile a template into the given file.
		 * Creates a file with the given name, that can be loaded as a 
		 * template by this class and will contain the header, footer 
		 * and item templates as provided.
		 * \param filename name of the file to save the compiled template to
		 * \param header contents of the header template
		 * \param footer contents of the footer template
		 * \param item contents of the item template */
		static void compile(const boost::filesystem::path & filename, const std::string & header, const std::string & footer, const std::string & item);

	private :
		struct TemplateHeader
		{
			unsigned int header_offset_;
			unsigned int header_size_;
			unsigned int footer_offset_;
			unsigned int footer_size_;
			unsigned int item_offset_;
			unsigned int item_size_;
		};

		std::string header_;
		std::string footer_;
		std::string item_;
	};
}

#endif
