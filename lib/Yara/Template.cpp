/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Template.h"
#include <fstream>
#include <iterator>
#include <vector>
#include <boost/filesystem/path.hpp>

namespace Yara
{
	Template::Template()
	{ /* no-op */ }

	Template::Template(const boost::filesystem::path & filename)
	{
		std::ifstream in_file(filename.string().c_str(), std::ios::binary);
		in_file.exceptions(std::ios::badbit | std::ios::failbit);
		TemplateHeader template_header;
		{
			char * p(reinterpret_cast<char *>(&template_header));
			in_file.read(p, sizeof(TemplateHeader));
		}
		{
			std::vector< char > buffer(template_header.header_size_);
			in_file.seekg(template_header.header_offset_);
			in_file.read(&(buffer[0]), template_header.header_size_);
			header_ = std::string(buffer.begin(), buffer.end());
		}
		{
			std::vector< char > buffer(template_header.footer_size_);
			in_file.seekg(template_header.footer_offset_);
			in_file.read(&(buffer[0]), template_header.footer_size_);
			footer_ = std::string(buffer.begin(), buffer.end());
		}
		{
			std::vector< char > buffer(template_header.item_size_);
			in_file.seekg(template_header.item_offset_);
			in_file.read(&(buffer[0]), template_header.item_size_);
			item_ = std::string(buffer.begin(), buffer.end());
		}
	}

	Template::~Template()
	{ /* no-op */ }

	void Template::compile(const boost::filesystem::path & filename, const std::string & header, const std::string & footer, const std::string & item)
	{
		TemplateHeader template_header;
		template_header.header_offset_ = sizeof(TemplateHeader);
		template_header.header_size_ = static_cast< unsigned int >(header.size());
		template_header.footer_offset_ = template_header.header_offset_ + template_header.header_size_;
		template_header.footer_size_ = static_cast< unsigned int >(footer.size());
		template_header.item_offset_ = template_header.footer_offset_ + template_header.footer_size_;
		template_header.item_size_ = static_cast< unsigned int >(item.size());

		std::ofstream out_file(filename.string().c_str(), std::ios::binary);
		{
			std::vector< char > buffer;
			char * p(reinterpret_cast<char *>(&template_header));
			std::copy(p, p + sizeof(TemplateHeader), std::back_inserter(buffer));
			out_file.write(&(buffer[0]), static_cast< std::streamsize >(buffer.size()));
		}
		out_file.write(&(header[0]), static_cast< std::streamsize >(header.size()));
		out_file.write(&(footer[0]), static_cast< std::streamsize >(footer.size()));
		out_file.write(&(item[0]), static_cast< std::streamsize >(item.size()));
	}
}
