/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_renderer_h
#define _yara_renderer_h
/** \file Yara/Renderer.h Defines the Renderer class.
 * Part of the C++ API for Yara, defines the class that renders the stream
 * into the Template, from the information given by the Parser class. */
#ifdef DOXYGEN_GENERATING
#include <Yara/Renderer.h>
#else
#include "Details/prologue.h"
#include <ostream>
#endif

namespace Yara
{
	class Parser;
	class Template;
	namespace Details
	{
		struct ChannelInfo;
		struct ItemInfo;
	}
	/** \brief Render an RSS stream with the given template, from the given parser.
	 * This class takes the template and can take the output of any number 
	 * of parsers to render the streams they parsed into the same number 
	 * of streams.
	 *
	 * As the Parser class retains the information it found in the RSS stream,
	 * the Renderer class is essentially stateless. The only thing it knows
	 * about is the instance of the Template class it should use for rendering. */
	class Yara_API Renderer
	{
	public :
		//! Construct an instance from a template. Keeps a copy of the template.
		Renderer(const Template & _template);
		~Renderer();

		//! Render the information from a given Parser into the given stream, using the instance's template.
		void render(std::ostream & target, const Parser & parser);

	private :
		struct Data;

		// Neither CopyConstructible nor Assignable
		Renderer(const Renderer &);
		Renderer & operator=(const Renderer &);

		Data * data_;
	};
}

#endif
