/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Yara.h"
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <fstream>
#include <string>
#include <stack>
#include <stdexcept>
#include <vector>
#include "Parser.h"
#include "Template.h"
#include "Renderer.h"
#include "Details/ItemInfo.h"
#include "Details/ImageInfo.h"
#include "Details/ChannelInfo.h"
#include "Generator.h"

namespace
{
	std::stack< std::string > errors__;
}

struct YaraItemInfo_ : Yara::Details::ItemInfo
{};

struct YaraImageInfo_ : Yara::Details::ImageInfo
{};

struct YaraChannelInfo_ : Yara::Details::ChannelInfo
{
	std::string temp_;
};

struct YaraGenerator_ : Yara::Generator
{};

extern "C" int Yara_render(const char * target_filename, const char * template_filename, const char * rss_xml)	try
{
	Yara::Parser parser;
	parser.parse(rss_xml);
	Yara::Renderer renderer(Yara::Template(boost::filesystem::path(template_filename, boost::filesystem::native)));
	std::ofstream target_file(target_filename, std::ios::binary);
	renderer.render(target_file, parser);

	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}

extern "C" int Yara_compileTemplate(const char * _target_filename, const char * _header_filename, const char * _footer_filename, const char * _item_filename) try
{
	boost::filesystem::path target_filename(_target_filename, boost::filesystem::native);
	boost::filesystem::path header_filename(_header_filename, boost::filesystem::native);
	boost::filesystem::path footer_filename(_footer_filename, boost::filesystem::native);
	boost::filesystem::path item_filename(_item_filename, boost::filesystem::native);
	std::ifstream header_file(header_filename.string().c_str(), std::ios::in| std::ios::binary| std::ios::ate);
	header_file.exceptions(std::ios::badbit | std::ios::failbit);
	if (!header_file)
		throw std::runtime_error("Failed to open header file");
	else
	{ /* all is well */ }
	std::size_t header_file_size(header_file.tellg());
	header_file.seekg(0, std::ios::beg);
	std::vector< char > header_data(header_file_size);
	assert(static_cast< std::streamsize >(header_file_size) == header_file_size);
	header_file.read(&(header_data[0]), static_cast< std::streamsize >(header_file_size));

	std::ifstream footer_file(footer_filename.string().c_str(), std::ios::in| std::ios::binary| std::ios::ate);
	footer_file.exceptions(std::ios::badbit | std::ios::failbit);
	if (!footer_file)
		throw std::runtime_error("Failed to open footer file");
	else
	{ /* all is well */ }
	std::size_t footer_file_size(footer_file.tellg());
	footer_file.seekg(0, std::ios::beg);
	std::vector< char > footer_data(footer_file_size);
	assert(static_cast< std::streamsize >(footer_file_size) == footer_file_size);
	footer_file.read(&(footer_data[0]), static_cast< std::streamsize >(footer_file_size));

	std::ifstream item_file(item_filename.string().c_str(), std::ios::in| std::ios::binary| std::ios::ate);
	item_file.exceptions(std::ios::badbit | std::ios::failbit);
	if (!item_file)
		throw std::runtime_error("Failed to open item file");
	else
	{ /* all is well */ }
	std::size_t item_file_size(item_file.tellg());
	item_file.seekg(0, std::ios::beg);
	std::vector< char > item_data(item_file_size);
	assert(static_cast< std::streamsize >(item_file_size) == item_file_size);
	item_file.read(&(item_data[0]), static_cast< std::streamsize >(item_file_size));

	Yara::Template::compile(target_filename, std::string(header_data.begin(), header_data.end()), std::string(footer_data.begin(), footer_data.end()), std::string(item_data.begin(), item_data.end()));

	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * Yara_getLastError()
{
	if (!errors__.empty())
		return errors__.top().c_str();
	else return 0;
}

extern "C" void Yara_popLastError()
{
	if (!errors__.empty())
		errors__.pop();
	else
	{ /* nothing to do */ }
}

extern "C" YaraItemInfo * YaraItemInfo_new() try
{
	return new YaraItemInfo;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" void YaraItemInfo_delete(YaraItemInfo * item_info)
{
	delete item_info;
}

extern "C" int YaraItemInfo_setTitle(YaraItemInfo * item_info, const char * title) try
{
	item_info->title_ = title;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraItemInfo_getTitle(YaraItemInfo * item_info) try
{
	return item_info->title_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraItemInfo_setLink(YaraItemInfo * item_info, const char * link) try
{
	item_info->link_ = link;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraItemInfo_getLink(YaraItemInfo * item_info) try
{
	return item_info->link_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraItemInfo_setDescription(YaraItemInfo * item_info, const char * description) try
{
	item_info->description_ = description;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraItemInfo_getDescription(YaraItemInfo * item_info) try
{
	return item_info->description_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" YaraImageInfo * YaraImageInfo_new() try
{
	return new YaraImageInfo;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" void YaraImageInfo_delete(YaraImageInfo * image_info)
{
	delete image_info;
}

extern "C" int YaraImageInfo_setTitle(YaraImageInfo * image_info, const char * title) try
{
	image_info->title_ = title;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraImageInfo_getTitle(YaraImageInfo * image_info) try
{
	return image_info->title_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraImageInfo_setURL(YaraImageInfo * image_info, const char * url) try
{
	image_info->url_ = url;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraImageInfo_getURL(YaraImageInfo * image_info) try
{
	return image_info->url_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraImageInfo_setLink(YaraImageInfo * image_info, const char * link) try
{
	image_info->link_ = link;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraImageInfo_getLink(YaraImageInfo * image_info) try
{
	return image_info->link_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraImageInfo_setDescription(YaraImageInfo * image_info, const char * description) try
{
	image_info->description_ = description;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraImageInfo_getDescription(YaraImageInfo * image_info) try
{
	return image_info->description_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraImageInfo_setWidth(YaraImageInfo * image_info, unsigned int width) try
{
	image_info->width_ = width;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" unsigned int YaraImageInfo_getWidth(YaraImageInfo * image_info) try
{
	return image_info->width_;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" int YaraImageInfo_setHeight(YaraImageInfo * image_info, unsigned int height) try
{
	image_info->height_ = height;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" unsigned int YaraImageInfo_getHeight(YaraImageInfo * image_info) try
{
	return image_info->height_;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" YaraChannelInfo * YaraChannelInfo_new() try
{
	return new YaraChannelInfo;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" void YaraChannelInfo_delete(YaraChannelInfo * channel_info) 
{
	delete channel_info;
}

extern "C" int YaraChannelInfo_setTitle(YaraChannelInfo * channel_info, const char * title) try
{
	channel_info->title_ = title;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraChannelInfo_getTitle(YaraChannelInfo * channel_info) try
{
	return channel_info->title_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setLink(YaraChannelInfo * channel_info, const char * link) try
{
	channel_info->link_ = link;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraChannelInfo_getLink(YaraChannelInfo * channel_info) try
{
	return channel_info->link_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setDescription(YaraChannelInfo * channel_info, const char * description) try
{
	channel_info->description_ = description;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraChannelInfo_getDescription(YaraChannelInfo * channel_info) try
{
	return channel_info->description_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setLanguage(YaraChannelInfo * channel_info, const char * language) try
{
	channel_info->language_ = Yara::Details::getLanguage(language);
	if (channel_info->language_ == Yara::Details::unknown__)
	{
		errors__.push("Unknown language");
		return -1;
	}
	else
		return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" const char * YaraChannelInfo_getLanguage(YaraChannelInfo * channel_info) try
{
	channel_info->temp_ = getLanguage(channel_info->language_);
	return channel_info->temp_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setCopyright(YaraChannelInfo * channel_info, const char * copyright) try
{
	channel_info->copyright_ = copyright;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraChannelInfo_getCopyright(YaraChannelInfo * channel_info) try
{
	return channel_info->copyright_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setManagingEditor(YaraChannelInfo * channel_info, const char * managing_editor) try
{
	channel_info->managing_editor_ = managing_editor;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraChannelInfo_getManagingEditor(YaraChannelInfo * channel_info) try
{
	return channel_info->managing_editor_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setWebmaster(YaraChannelInfo * channel_info, const char * webmaster) try
{
	channel_info->webmaster_ = webmaster;
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" const char * YaraChannelInfo_getWebmaster(YaraChannelInfo * channel_info) try
{
	return channel_info->webmaster_.c_str();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" int YaraChannelInfo_setImage(YaraChannelInfo * channel_info, YaraImageInfo * image) try
{
	channel_info->image_.reset(new YaraImageInfo(*image));
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" YaraImageInfo * YaraChannelInfo_getImage(YaraChannelInfo * channel_info) try
{
	return static_cast< YaraImageInfo* >(channel_info->image_.get());
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" YaraGenerator * YaraGenerator_new() try
{
	return new YaraGenerator;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}

extern "C" void YaraGenerator_delete(YaraGenerator * generator) 
{
	delete generator;
}

extern "C" int YaraGenerator_setChannelInfo(YaraGenerator * generator, YaraChannelInfo * channel_info) try
{
	generator->setChannelInfo(*channel_info);
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}

extern "C" void YaraGenerator_reset(YaraGenerator * generator)
{
	generator->reset();
}

extern "C" int YaraGenerator_addItem(YaraGenerator * generator, YaraItemInfo * item) try
{
	generator->addItem(*item);
	return 0;
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return -1;
}
extern "C" const char * YaraGenerator_generate(YaraGenerator * generator)  try
{
	return generator->generate();
}
catch (const std::exception & e)
{
	try
	{
		errors__.push(e.what());
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;	
}
catch (...)
{
	try
	{
		errors__.push("Unknown error");
	}
	catch (...)
	{ /* nothing I can do here */ }
	return 0;
}
