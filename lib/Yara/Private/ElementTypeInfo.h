/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_elementtypeinfo_h
#define _yara_details_elementtypeinfo_h

#include <string>
#include <vector>
#include <utility>
#include "../Details/ElementType.h"
#include "Callbacks.h"

namespace Yara
{
	namespace Private
	{
		struct ElementTypeInfo
		{
			ElementTypeInfo(
				Details::ElementType element_type, 
				ElementParsingCallback element_parsing_callback, 
				DataParsingCallback data_handling_callback,
				EndOfElementCallback end_of_element_callback)
				: element_type_(element_type),
				  element_parsing_callback_(element_parsing_callback),
				  data_handling_callback_(data_handling_callback),
				  end_of_element_callback_(end_of_element_callback)
			{ /* no-op */ }

			Details::ElementType element_type_;
			ElementParsingCallback element_parsing_callback_;
			DataParsingCallback data_handling_callback_;
			EndOfElementCallback end_of_element_callback_;
		};
		ElementTypeInfo getElementTypeInfo(Details::ElementType current, const char * element_name);
	}
}


#endif
