/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Callbacks.h"
#include "ParserData.h"
#include "../Exceptions/ParserExceptions.h"

namespace Yara
{
	namespace Private
	{
		void interpreteRSSAttributes__(Yara::Private::ParserData & parser_data, const Yara::Private::Attributes & attributes)
		{
			bool version_found(false);
			using namespace Yara::Private;
			// we will ignore all attributes except version. Version should be 0.91, 0.92 or 2.0
			Attributes::const_iterator end(attributes.end());
			for (Attributes::const_iterator where(attributes.begin()); where != end; ++where)
			{
				if (where->first == "version" && (where->second != "0.91" && where->second != "0.92" && where->second != "2.0"))
					throw Yara::Exceptions::RSSVersionError(where->second);
				else if (where->first == "version")
				{
					version_found = true;
					if (parser_data.rss_version_.empty())
						parser_data.rss_version_ = where->second;
					else
						throw Yara::Exceptions::RSSVersionError("<duplicate version attribute?>");	
				}
			}
			if (!version_found)
				throw Yara::Exceptions::RSSVersionError("<none>");
			else
			{ /* all is well */ }
		}

		void handleImageStart__(Yara::Private::ParserData & parser_data, const Yara::Private::Attributes & attributes)
		{
			parser_data.current_image_info_.reset(new Yara::Details::ImageInfo);
		}

		void handleItemStart__(Yara::Private::ParserData & parser_data, const Yara::Private::Attributes & attributes)
		{
			parser_data.current_item_info_.reset(new Yara::Details::ItemInfo);
		}
	}
}
