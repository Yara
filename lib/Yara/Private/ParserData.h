/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_private_parserdata_h
#define _yara_private_parserdata_h

#include <stack>
#include "../Details/ElementType.h"
#include "../Details/Language.h"
#include "../Details/ChannelInfo.h"
#include "../Details/ImageInfo.h"
#include "../Details/ItemInfo.h"
#include "Callbacks.h"

namespace Yara
{
	namespace Private
	{
		struct ParserData
		{
			std::stack< Details::ElementType > current_element_;
			std::stack< DataParsingCallback > data_parsing_callbacks_;
			std::stack< EndOfElementCallback > end_of_element_callbacks_;

			// information we've gathered about the RSS stream we've been parsing
			std::string rss_version_;
			Details::ChannelInfo channel_info_;
			std::vector< boost::shared_ptr< Details::ItemInfo > > items_;

			// working buffers for element data handlers
			std::vector< char > buffer_;
			std::auto_ptr< Details::ImageInfo > current_image_info_;
			std::auto_ptr< Details::ItemInfo > current_item_info_;
		};
	}
}

#endif
