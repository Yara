/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Callbacks.h"
#include <cassert>
#include <boost/lexical_cast.hpp>
#include "ParserData.h"
#include "../Exceptions/ParserExceptions.h"

namespace Yara
{
	namespace Private
	{
		void handleStringElementData__(ParserData & parser_data, const std::vector< char > & data, bool at_end)
		{
			if (!at_end)
				std::copy(data.begin(), data.end(), std::back_inserter(parser_data.buffer_));
			else
			{
				// we're handling data here, but we need to know for which element
				assert(!parser_data.current_element_.empty());
				switch (parser_data.current_element_.top())
				{
				case Details::channel_title__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					if (parser_data.channel_info_.title_.empty())
						parser_data.channel_info_.title_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::channel_link__ :
					// there can't be any XML elements in the link (we don't allow it) so we will throw an exception
					if (parser_data.channel_info_.link_.empty())
						parser_data.channel_info_.link_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::channel_description__ :
					// there can't be any XML elements in the description (we don't allow it) so we will throw an exception
					if (parser_data.channel_info_.description_.empty())
						parser_data.channel_info_.description_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::channel_copyright__ :
					// there can't be any XML elements in the description (we don't allow it) so we will throw an exception
					if (parser_data.channel_info_.copyright_.empty())
						parser_data.channel_info_.copyright_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::managing_editor__ :
					// there can't be any XML elements in the description (we don't allow it) so we will throw an exception
					if (parser_data.channel_info_.managing_editor_.empty())
						parser_data.channel_info_.managing_editor_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::webmaster__ :
					// there can't be any XML elements in the description (we don't allow it) so we will throw an exception
					if (parser_data.channel_info_.webmaster_.empty())
						parser_data.channel_info_.webmaster_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::image_title__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_image_info_.get());
					if (parser_data.current_image_info_->title_.empty())
						parser_data.current_image_info_->title_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::image_url__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_image_info_.get());
					if (parser_data.current_image_info_->url_.empty())
						parser_data.current_image_info_->url_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::image_link__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_image_info_.get());
					if (parser_data.current_image_info_->link_.empty())
						parser_data.current_image_info_->link_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::image_description__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_image_info_.get());
					if (parser_data.current_image_info_->description_.empty())
						parser_data.current_image_info_->description_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::item_title__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_item_info_.get());
					if (parser_data.current_item_info_->title_.empty())
						parser_data.current_item_info_->title_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::item_link__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_item_info_.get());
					if (parser_data.current_item_info_->link_.empty())
						parser_data.current_item_info_->link_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				case Details::item_description__ :
					// there can't be any XML elements in the title (we don't allow it) so we will throw an exception
					assert(parser_data.current_item_info_.get());
					if (parser_data.current_item_info_->description_.empty())
						parser_data.current_item_info_->description_ = std::string(parser_data.buffer_.begin(), parser_data.buffer_.end());
					else
						throw Exceptions::RSSContentError(Exceptions::RSSContentError::unexpected_markup__);
					break;
				default :
					// ignore all others for now - we did it all for naught
					;
				}
			}
		}

		void handleLanguageElementData__(ParserData & parser_data, const std::vector< char > & data, bool at_end)
		{
			if (!at_end)
				std::copy(data.begin(), data.end(), std::back_inserter(parser_data.buffer_));
			else
			{
				Details::Language language(Details::getLanguage(std::string(parser_data.buffer_.begin(), parser_data.buffer_.end())));
				// we're handling description data here, but we need to know which description
				assert(!parser_data.current_element_.empty());
				switch (parser_data.current_element_.top())
				{
				case Details::channel_language__ :
					// there is no way we can check for an already-set language, so we don't
					parser_data.channel_info_.language_ = language;
					break;
				default :
					// ignore all others for now - we did it all for naught
					;
				}
			}
		}

		void handleUIntElementData__(ParserData & parser_data, const std::vector< char > & data, bool at_end)
		{
			if (!at_end)
				std::copy(data.begin(), data.end(), std::back_inserter(parser_data.buffer_));
			else
			{
				// we're handling description data here, but we need to know which description
				assert(!parser_data.current_element_.empty());
				switch (parser_data.current_element_.top())
				{
				case Details::image_width__ :
					assert(parser_data.current_image_info_.get());
					parser_data.current_image_info_->width_ = boost::lexical_cast< unsigned int >(std::string(parser_data.buffer_.begin(), parser_data.buffer_.end()));
					break;
				case Details::image_height__ :
					assert(parser_data.current_image_info_.get());
					parser_data.current_image_info_->height_ = boost::lexical_cast< unsigned int >(std::string(parser_data.buffer_.begin(), parser_data.buffer_.end()));
					break;
				default :
					// ignore all others for now - we did it all for naught
					;
				}
			}
		}
	}
}
