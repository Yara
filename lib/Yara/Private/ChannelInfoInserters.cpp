/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RenderingChannelInserter.h"
#include "../Details/ChannelInfo.h"

namespace Yara
{
	namespace Private
	{
		void insertChannelTitle__(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			target << channel_info.title_;
		}

		void insertChannelLink__(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			target << channel_info.link_;
		}

		void insertChannelDescription__(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			target << channel_info.description_;
		}

		void insertChannelLanguage__(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			target << channel_info.language_;
		}

		void insertChannelCopyright(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			target << channel_info.copyright_;
		}

		void insertChannelManagingEditor__(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			channel_info.managing_editor_;
		}

		void insertChannelWebmaster__(std::ostream & target, const Details::ChannelInfo & channel_info)
		{
			channel_info.webmaster_;
		}
	}
}
