/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_private_callbacks_h
#define _yara_private_callbacks_h

#include "Attributes.h"

namespace Yara
{
	namespace Private
	{
		struct ParserData;
		// types of the call-backs to be used by the parser
		// In the gperf-generated hash, there are two pointers to callbacks 
		// for each entry: one callback to parse the element itself (namely 
		// the attributes) and one to parse the character data that comes 
		// with it. The latter is called on three conditions by the parser: 
		// when new character data is found (in which case the bool at the end
		// of the argument list is false), or when the element either ends or
		// is interrupted by a new elemenent, in which case the bool at the
		// end is true.
		typedef void (*ElementParsingCallback)(ParserData &, const Attributes &);
		typedef void (*DataParsingCallback)(ParserData &, const std::vector< char > &, bool);
		typedef void (*EndOfElementCallback)(ParserData &);

		// element-parsing callbacks
		void interpreteRSSAttributes__(Yara::Private::ParserData & parser_data, const Yara::Private::Attributes & attributes);
		void handleImageStart__(Yara::Private::ParserData & parser_data, const Yara::Private::Attributes & attributes);
		void handleItemStart__(Yara::Private::ParserData & parser_data, const Yara::Private::Attributes & attributes);

		// data-handling callbacks
		void handleStringElementData__(ParserData & parser_data, const std::vector< char > & data, bool at_end);
		void handleLanguageElementData__(ParserData & parser_data, const std::vector< char > & data, bool at_end);
		void handleUIntElementData__(ParserData & parser_data, const std::vector< char > & data, bool at_end);

		// end-of-element callbacks
		void handleEndOfElement__(ParserData & parser_data);
	}
}

#endif
