/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RenderingItemInserter.h"
#include "../Details/ItemInfo.h"

namespace Yara
{
	namespace Private
	{
		void insertItemTitle__(std::ostream & target, const Details::ItemInfo & item_info)
		{
			target << item_info.title_;
		}

		void insertItemLink__(std::ostream & target, const Details::ItemInfo & item_info)
		{
			target << item_info.link_;
		}

		void insertItemDescription__(std::ostream & target, const Details::ItemInfo & item_info)
		{
			target << item_info.description_;
		}
	}
}
