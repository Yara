/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_private_renderingiteminserter_h
#define _yara_private_renderingiteminserter_h

#include <ostream>
#include <boost/function.hpp>

namespace Yara
{
	namespace Details
	{
		struct ItemInfo;
	}
	namespace Private
	{
		class ParserData;
		typedef void(*RenderingItemInserter)(std::ostream &, const Details::ItemInfo &);

		RenderingItemInserter getRenderingItemInserter(const std::string & tag);

		void insertItemTitle__(std::ostream &, const Details::ItemInfo &);
		void insertItemLink__(std::ostream &, const Details::ItemInfo &);
		void insertItemDescription__(std::ostream &, const Details::ItemInfo &);
	}
}

#endif
