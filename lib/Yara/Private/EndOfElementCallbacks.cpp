/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Callbacks.h"
#include <cassert>
#include "ParserData.h"
#include "../Details/ElementType.h"
#include "../Exceptions/ParserExceptions.h"

namespace Yara
{
	namespace Private
	{
		void handleEndOfElement__(ParserData & parser_data)
		{
			using namespace Details;
			assert(!parser_data.current_element_.empty());
			switch (parser_data.current_element_.top())
			{
			case channel_image__ :
				parser_data.channel_info_.image_ = parser_data.current_image_info_;
				break;
			case item__ :
			{
				boost::shared_ptr< ItemInfo > item(parser_data.current_item_info_);
				parser_data.items_.push_back(item);
				break;
			}
			default :
				/* nothing to do for any other element type */
				;
			};
		}
	}
}
