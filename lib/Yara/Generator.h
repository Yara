/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_generator_h
#define _yara_generator_h
/** \file Yara/Generator.h Defines the Generator class.
 * Part of the C++ API for Yara, defines the class that generates an RSS
 * stream from any number of items and a channel description */
#ifdef DOXYGEN_GENERATING
#include <Yara/Generator.h>
#else
#include "Details/prologue.h"
#include <iostream>
#include <string>
#include <vector>
#include "Details/Language.h"
#include "Details/ChannelInfo.h"
#include "Details/ItemInfo.h"
#endif

namespace Yara
{
	/** An RSS stream generator for version 2.0 of the specification. */
	class Yara_API Generator
	{
	public :
		//! Construct a generator that is ready to generate a feed
		Generator();
		~Generator();

		/** Generate a feed.
		 * This will generate an XML stream with the items and the channel info 
		 * that have been added to the generator so far. The generator will keep 
		 * a copy of the stream internally and return a reference to itself, which 
		 * you can subsequently cast to a const char *. This is to allow for easy 
		 * use from the C interface. */
		Generator & generate();

		operator const char * () const;

		/** Reset the generator and clear its contents.
		 * Resets the state of the generator, after which it will be able to generate 
		 * from fresh input. */
		void reset();

		//! Get the number of items that will be generated
		unsigned int getItemCount() const { return items_.size(); }

		/** Set the channel info */
		void setChannelInfo(const Details::ChannelInfo & channel_info) { channel_info_ = channel_info; serialized_feed_.clear(); }
		/** Add an item to the generator */
		void addItem(const Details::ItemInfo & item) { items_.push_back(item); serialized_feed_.clear(); }

	private :
		// neither CopyConstructible nor Assignable
		Generator(const Generator &);
		Generator& operator=(const Generator &);

		Details::ChannelInfo channel_info_;
		std::vector< Details::ItemInfo > items_;

		std::string serialized_feed_;
	};
}

#endif
