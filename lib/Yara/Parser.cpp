/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Parser.h"
#include <cassert>
#include <stack>
#include <utility>
#include <boost/static_assert.hpp>
extern "C" {
#	include <expat.h>
}
#include "Exceptions/ParserExceptions.h"
#include "Private/ElementTypeInfo.h"
#include "Private/ParserData.h"

namespace Yara
{
	namespace 
	{
		void XMLCALL handleStartElement(void * user_data, const char * name, const char **attributes)
		{
			Private::ParserData * rss_data((Private::ParserData *)user_data);
			// tell data handlers to flush their buffers
			if (!rss_data->data_parsing_callbacks_.empty() && rss_data->data_parsing_callbacks_.top())
				rss_data->data_parsing_callbacks_.top()(*rss_data, std::vector< char >(), true);
			else
			{ /* no data handler */ }
			rss_data->buffer_.clear();

			// find new element data
			Private::ElementTypeInfo element_type_info(Private::getElementTypeInfo(rss_data->current_element_.empty() ? Details::none__ : rss_data->current_element_.top(), name));
			if (element_type_info.element_parsing_callback_)
			{
				Private::Attributes atts;
				while (attributes && *attributes)
				{
					if (attributes[1])
					{	// attribute with value
						atts.push_back( std::make_pair( attributes[0], attributes[1] ) );
					}
					else
					{
						atts.push_back( std::make_pair( attributes[0], std::string() ) );
					}
					std::advance(attributes, 2);
				};
				element_type_info.element_parsing_callback_(*rss_data, atts);
			}
			else
			{ /* no call-back to call for validation */ }
			rss_data->current_element_.push(element_type_info.element_type_);
			rss_data->data_parsing_callbacks_.push(element_type_info.data_handling_callback_);
			rss_data->end_of_element_callbacks_.push(element_type_info.end_of_element_callback_);
		}

		void XMLCALL handleEndElement(void * user_data, const char * name)
		{
			Private::ParserData * rss_data((Private::ParserData *)user_data);
			// tell data handlers to flush their buffers
			if (rss_data->data_parsing_callbacks_.top())
				rss_data->data_parsing_callbacks_.top()(*rss_data, std::vector< char >(), true);
			else
			{ /* no data handler */ }
			if (rss_data->end_of_element_callbacks_.top())
				rss_data->end_of_element_callbacks_.top()(*rss_data);
			else
			{ /* no data handler */ }
			rss_data->buffer_.clear();
			rss_data->current_element_.pop();
			rss_data->data_parsing_callbacks_.pop();
			rss_data->end_of_element_callbacks_.pop();
		}

		void XMLCALL handleCharacterData(void * user_data, const char * data, int len)
		{
			Private::ParserData * rss_data((Private::ParserData *)user_data);
			// tell data handlers to flush their buffers
			if (!rss_data->data_parsing_callbacks_.empty() && rss_data->data_parsing_callbacks_.top())
				rss_data->data_parsing_callbacks_.top()(*rss_data, std::vector< char >(data, data + len), false);
			else
			{ /* no data handler */ }
		}
	}

	struct Parser::Data
	{
		Data()
			: ready_(true),
			  expat_parser_(XML_ParserCreate(NULL))
		{
			if (!expat_parser_)
				throw std::bad_alloc();
			XML_SetUserData(expat_parser_, &rss_data_);
			XML_SetElementHandler(expat_parser_, handleStartElement, handleEndElement);
			XML_SetCharacterDataHandler(expat_parser_, handleCharacterData);
		}

		~Data()
		{
			XML_ParserFree(expat_parser_);
		}

		XML_Parser expat_parser_;
		bool ready_;
		Private::ParserData rss_data_;
	};

	Parser::Parser()
		: data_(new Data)
	{ /* no-op */ }

	Parser::~Parser()
	{
		delete data_;
	}

	void Parser::parse(const std::string & rss_feed)
	{
		assert(static_cast<int>(rss_feed.size()) >= 0);
		if (data_->ready_)
		{
			BOOST_STATIC_ASSERT(sizeof(int) == sizeof(std::size_t));
			switch (XML_Parse(data_->expat_parser_, rss_feed.c_str(), static_cast<int>(rss_feed.size()), 0))
			{
			case XML_STATUS_OK :
				/* all is well */
				break;
			case XML_STATUS_ERROR :
				{
				int error_code(XML_GetErrorCode(data_->expat_parser_));
				throw Exceptions::XMLError(XML_GetCurrentLineNumber(data_->expat_parser_), XML_GetCurrentColumnNumber(data_->expat_parser_), XML_GetCurrentByteIndex(data_->expat_parser_));
				}
			case XML_STATUS_SUSPENDED :
				throw Exceptions::InternalError(Exceptions::InternalError::xml_parser_suspended__);
			}
		}
		else
			throw Exceptions::ParserNotReady();
	}

	bool Parser::finish()
	{
		return finish_(NoThrow());
	}

	Parser::operator Parser::Bool ()
	{
		return data_->ready_ ? &Parser::unused_except_for_cast_ : 0;
	}

	void Parser::reset()
	{
		Data * data(new Data);
		std::swap(data_, data);
		delete data;
	}

	std::string Parser::getRSSVersion() const
	{
		finish_();
		return data_->rss_data_.rss_version_;
	}

	Details::ChannelInfo Parser::getChannelInfo() const
	{
		finish_();
		return data_->rss_data_.channel_info_;
	}

	unsigned int Parser::getItemCount() const
	{
		finish_();
		assert(static_cast< unsigned int >(data_->rss_data_.items_.size()) == data_->rss_data_.items_.size());
		return static_cast< unsigned int >(data_->rss_data_.items_.size());
	}

	Details::ItemInfo Parser::getItem(unsigned int index) const
	{
		finish_();
		return *(data_->rss_data_.items_.at(index));
	}

	void Parser::finish_() const
	{
		if (data_->ready_)
		{
			if (!finish_(NoThrow()))
				throw Exceptions::ParserNotDone();
			else
			{ /* all is well */ }
		}
		else
		{ /* already finished */ }
	}

	bool Parser::finish_(const Parser::NoThrow &) const
	{
		data_->ready_ = false;
		switch (XML_Parse(data_->expat_parser_, "", 0, 1))
		{
		case XML_STATUS_OK :
			break;
		case XML_STATUS_ERROR :
			{
				int error_code(XML_GetErrorCode(data_->expat_parser_));
				if (error_code == XML_ERROR_NO_ELEMENTS ||
					error_code == XML_ERROR_UNCLOSED_TOKEN ||
					error_code == XML_ERROR_UNCLOSED_CDATA_SECTION ||
					error_code == XML_ERROR_INCOMPLETE_PE)
					return false;
				else
					throw Exceptions::XMLError(XML_GetCurrentLineNumber(data_->expat_parser_), XML_GetCurrentColumnNumber(data_->expat_parser_), XML_GetCurrentByteIndex(data_->expat_parser_));
			}
		case XML_STATUS_SUSPENDED :
			throw Exceptions::InternalError(Exceptions::InternalError::xml_parser_suspended__);
		default :
			throw Exceptions::InternalError(Exceptions::InternalError::xml_parser_returned_unknown_value__);
		}
		return true;
	}

	Parser & operator>>(std::istream & is, Parser & parser)
	{
		return parser;
	}
}
