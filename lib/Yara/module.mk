# the library
Yara_SRC =						\
	Details/DateParser.cpp				\
	Details/Language.cpp				\
	Parser.cpp					\
	Private/ChannelInfoInserters.cpp		\
	Private/DataHandlingCallbacks.cpp		\
	Private/ElementParsingCallbacks.cpp		\
	Private/EndOfElementCallbacks.cpp		\
	Private/ItemInfoInserters.cpp			\
	Renderer.cpp					\
	Template.cpp					\
	Yara.cpp					\
	Details/Language.cpp.gperf			\
	Private/ElementTypeInfo.cpp.gperf		\
	Private/RenderingChannelInserter.cpp.gperf	\
	Private/RenderingItemInserter.cpp.gperf		\
	Generator.cpp					\

Yara_INSTALL_HEADERS +=					\
	Details/ChannelInfo.h				\
	Details/DateParser.h				\
	Details/ElementType.h				\
	Details/ImageInfo.h				\
	Details/ItemInfo.h				\
	Details/Language.h				\
	Details/documentation.h				\
	Details/prologue.h				\
	Exceptions/ParserExceptions.h			\
	Parser.h					\
	Renderer.h					\
	Template.h					\
	Yara.h						\
	Generator.h					\
	Exceptions/GeneratorExceptions.h

SRC += $(patsubst %,lib/Yara/%,$(Yara_SRC))

INSTALL_HEADERS += $(patsubst %,Yara/%,$(Yara_INSTALL_HEADERS))
Yara_OBJ := $(patsubst %.cpp,lib/Yara/%.lo,$(filter %.cpp,$(Yara_SRC))) $(patsubst %.cpp.gperf,lib/Yara/%.lo,$(filter %.cpp.gperf,$(Yara_SRC)))
OBJ += $(Yara_OBJ)

$(eval $(call LINK_LIBRARY_template,libYara.la,$(Yara_OBJ)))
