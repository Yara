/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_parser_h
#define _yara_parser_h
/** \file Yara/Parser.h Defines the Parser class.
 * Part of the C++ API for Yara, defines the class that renders the stream
 * into the Template, from the information given by the Parser class. */
#ifdef DOXYGEN_GENERATING
#include <Yara/Parser.h>
#else
#include "Details/prologue.h"
#include <iostream>
#include <string>
#include "Details/Language.h"
#include "Details/ChannelInfo.h"
#include "Details/ItemInfo.h"
#endif

namespace Yara
{
	class Parser;

	//! Parse everything there is to parse in an input stream.
	Yara_API Parser & operator>>(std::istream & is, Parser & parser);

	/** An full-fledged RSS parser for versions 0.92, 0.93 and 2.0 of the 
	 * specification. */
	class Yara_API Parser
	{
	public :
		//! A type convertible to bool but with no other utility
		typedef int Parser::* Bool;

		//! Construct a parser that is ready to parse a feed
		Parser();
		~Parser();

		/** Parse a feed.
		 * Parsing a feed will add the data in it to the internal state of 
		 * the parser. You can only do this with a fresh parser or one on which
		 * you have called reset() since the last complete parse. If a parse
		 * is still en-route (because the previous parse was not complete)
		 * you can use this method to add chunks of the feed to the parse result.
		 * You can find out whether the parser is in a state in which it can parse
		 * by casting it to Bool.
		 * 
		 * \param rss_feed the feed to parse
		 * \throws a plethora of exceptions if the feed is not valid, all
		 *         directly or indirectly derived from std::exception. */
		void parse(const std::string & rss_feed);

		/** Finish parsing the internal buffers.
		 * Once all of the feed has been parsed, you can use this function to try
		 * and finish parsing. In stead of throwing an exception at you if it fails
		 * to finalize, it will tell you politely with a return value. Accessors
		 * that allow you to access the contents of the parsed feed will implicitly
		 * call this function if needed, but they will throw if the feed is not complete.
		 * Once this function is called, you can no longer add input to the parser without
		 * resetting first. */
		bool finish();

		/** Cast to Bool operator.
		 * Bool is convertible to bool, so you can use an instance of the parser
		 * in a branch expression (if, while, etc.). The result of this cast
		 * will be convertible to true if the parser is in a state in which it
		 * can parse more input, false otherwise. */
		operator Bool ();

		/** Reset the parser and clear its contents.
		 * Resets the state of the parser, after which it will be able to parse 
		 * fresh input. */
		void reset();

	/* querying the parse result */
		//! Get the version of the RSS stream we've passed, as reported by the stream itself
		std::string getRSSVersion() const;

		//! Get the title of the parsed channel
		Details::ChannelInfo getChannelInfo() const;

		//! Get the number of items parsed
		unsigned int getItemCount() const;
		//! Get an item by index
		Details::ItemInfo getItem(unsigned int index) const;
	private :
		struct Data;
		struct NoThrow {};

		// neither CopyConstructible nor Assignable
		Parser(const Parser &);
		Parser & operator=(const Parser &);

		void finish_() const;
		bool finish_(const NoThrow &) const;

		int unused_except_for_cast_;
		Data * data_;

		friend Yara_API Parser & operator>>(std::istream & is, Parser & parser);
	};
}

#endif
