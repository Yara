/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Renderer.h"
#include <algorithm>
#include <iterator>
#include <memory>
#include "Parser.h"
#include "Template.h"
#include "Private/RenderingChannelInserter.h"
#include "Private/RenderingItemInserter.h"

namespace Yara
{
	namespace
	{
		template < typename InfoType, typename InserterType >
		void render__(std::ostream & target, const InfoType & info, const std::string & input, InserterType (*getter)(const std::string &))
		{
			std::string::const_iterator begin(input.begin());
			std::string::const_iterator where(input.begin());
			std::string::const_iterator end(input.end());

			while (where != end)
			{
				std::string::size_type tag_start(input.find("$$__", std::distance(begin, where)));
				std::string::size_type tag_end(input.find("__$$", tag_start));
				if (tag_start != std::string::npos && tag_end != std::string::npos)
				{
					// when we get here, we can at least push out everything 
					// between the current value of where and the start of the
					// current tag
					std::copy(where, where + (tag_start - std::distance(begin, where)), std::ostream_iterator< char >(target));
					// the tag itself is between tag_start + 4 (the size of "$$__") and tag_end
					std::string tag(begin + tag_start + 4/* sizeof $$__ */, begin + tag_end);
					InserterType inserter(getter(tag));
					if (!inserter)
					{
						// we haven't found the tag. The policy is therefore to add it 
						// to the target stream and look again
						std::advance(where, tag_start);
						std::copy(where, where + 4/* sizeof $$__ */, std::ostream_iterator< char >(target));
						std::advance(where, 4/* sizeof $$__ */);
					}
					else
					{	// we've found a real tag. We'll use its inserter to add it to the target
						// stream and advance the where iterator to the end of the tag
						inserter(target, info);
						std::advance(where, tag_end + 4 - std::distance(begin, where));
					}
				}
				else
				{
					std::copy(where, end, std::ostream_iterator< char >(target));
					where = end;
				}
			}
		}
	}

	struct Renderer::Data
	{
		Data(const Template & _template)
			: template_(new Template(_template))
		{ /* no-op */ }

		std::auto_ptr< Template > template_;
	};

	Renderer::Renderer(const Template & _template)
		: data_(new Data(_template))
	{ /* no-op */ }

	Renderer::~Renderer()
	{
		delete data_;
	}

	void Renderer::render(std::ostream & target, const Parser & parser)
	{
		Details::ChannelInfo channel_info(parser.getChannelInfo());

		render__(target, channel_info, data_->template_->getHeader(), Private::getRenderingChannelInserter);
		unsigned int item_count(parser.getItemCount());
		for (unsigned int item_index(0); item_index < item_count; ++item_index)
		{
			Details::ItemInfo item(parser.getItem(item_index));
			render__(target, item, data_->template_->getItem(), Private::getRenderingItemInserter);
		}
		render__(target, channel_info, data_->template_->getFooter(), Private::getRenderingChannelInserter);
	}
}
