/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_language_h
#define _yara_details_language_h
/** \file Yara/Details/Language.h Defines the enumerator for all language codes and the function to convert one to the other. */
#include <string>

namespace Yara
{
	namespace Details
	{
		//! This enumeration contains a unique ID for each language code known to the RSS standard
		enum Language
		{
			afrikaans__,					// af
			albanian__,						// sq 
			basque__,						// eu
			belarusian__,					// be
			bulgarian__,					// bg
			catalan__,						// ca
			chinese_simplified__,			// zh-cn
			chinese_traditional__,			// zh-tw
			croatian__,						// hr
			czech__,						// cs
			danish__,						// da
			dutch__,						// nl
			dutch_belgium__,				// nl-be
			dutch_netherlands__,			// nl-nl
			english__,						// en
			english_australia__,			// en-au
			english_belize__,				// en-bz
			english_canada__,				// en-ca
			english_ireland__,				// en-ie
			english_jamaica__,				// en-jm
			english_new_zealand__,			// en-nz
			english_phillipines__,			// en-ph
			english_south_africa__,			// en-za
			english_trinidad__,				// en-tt
			english_united_kingdom__,		// en-gb
			english_united_states__,		// en-us
			english_zimbabwe__,				// en-zw
			estonian__,						// et
			faeroese__,						// fo
			finnish__,						// fi
			french__,						// fr
			french_belgium__,				// fr-be
			french_canada__,				// fr-ca
			french_france__,				// fr-fr
			french_luxembourg__,			// fr-lu
			french_monaco__,				// fr-mc
			french_switzerland__,			// fr-ch
			galician__,						// gl
			gaelic__,						// gd
			german__,						// de
			german_austria__,				// de-at
			german_germany__,				// de-de
			german_liechtenstein__,			// de-li
			german_luxembourg__,			// de-lu
			german_switzerland__,			// de-ch
			greek__,						// el
			hawaiian__,						// haw
			hungarian__,					// hu
			icelandic__,					// is
			indonesian__,					// in
			irish__,						// ga
			italian__,						// it
			italian_italy__,				// it-it
			italian_switzerland__,			// it-ch
			japanese__,						// ja
			korean__,						// ko
			macedonian__,					// mk
			norwegian__,					// no
			polish__,						// pl
			portuguese__,					// pt
			portuguese_brazil__,			// pt-br
			portuguese_portugal__,			// pt-pt
			romanian__,						// ro
			romanian_moldova__,				// ro-mo
			romanian_romania__,				// ro-ro
			russian__,						// ru
			russian_moldova__,				// ru-mo
			russian_russia__,				// ru-ru
			serbian__,						// sr
			slovak__,						// sk
			slovenian__,					// sl
			spanish__,						// es
			spanish_argentina__,			// es-ar
			spanish_bolivia__,				// es-bo
			spanish_chile__,				// es-cl
			spanish_colombia__,				// es-co
			spanish_costa_rica__,			// es-cr
			spanish_dominican_republic__,	// es-do
			spanish_ecuador__,				// es-ec
			spanish_el_salvador__,			// es-sv
			spanish_guatemala__,			// es-gt
			spanish_honduras__,				// es-hn
			spanish_mexico__,				// es-mx
			spanish_nicaragua__,			// es-ni
			spanish_panama__,				// es-pa
			spanish_paraguay__,				// es-py
			spanish_peru__,					// es-pe
			spanish_puerto_rico__,			// es-pr
			spanish_spain__,				// es-es
			spanish_uruguay__,				// es-uy
			spanish_venezuela__,			// es-ve
			swedish__,						// sv
			swedish_finland__,				// sv-fi
			swedish_sweden__,				// sv-se
			turkish__,						// tr
			ukranian__,						// uk

			unknown__
		};

		/** \brief Convert a language code to the enumerator value
		 * \returns the enumeration value of the language code, or unknown__ if unknown
		 * \param language_code the language code to translate */
		Language getLanguage(const std::string & language_code);

		std::string getLanguage(Language language);
	}
}

#endif
