/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "DateParser.h"
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/spirit.hpp>
#include <stdexcept>

namespace Yara
{
	namespace Details
	{
		struct DateParser::Grammar : public boost::spirit::grammar< DateParser::Grammar >
		{
			Grammar(Date & date)
				: date_(date)
			{ /* no-op */ }

			struct SetDayOfWeek
			{
				SetDayOfWeek(Date & date, int day_of_week)
					: date_(date),
					day_of_week_(day_of_week)
				{ /* no-op */ }

				const SetDayOfWeek & operator()(const char * begin, const char * end) const
				{
					date_.day_of_week_ = static_cast< Date::Day >(day_of_week_);
					return *this;
				}

				Date & date_;
				int day_of_week_;
			};

			struct SetMonth
			{
				SetMonth(Date & date, unsigned int month)
					: date_(date),
					  month_(month)
				{ /* no-op */ }

				const SetMonth & operator()(const char * begin, const char * end) const
				{
					date_.month_ = static_cast< Date::Month >(month_);
					return *this;
				}

				Date & date_;
				unsigned int month_;
			};

			struct SetTimeZone
			{
				SetTimeZone(Date & date, int hours, int minutes)
					: date_(date),
					  hours_(hours),
					  minutes_(minutes)
				{ /* no-op */ }

				const SetTimeZone & operator()(const char * begin, const char * end) const
				{
					date_.timezone_ = minutes_ + (60 * hours_);
					return *this;
				}

				Date & date_;
				int hours_;
				int minutes_;
			};

			void setDay(const char * begin, const char * end) const
			{
				date_.day_ = boost::lexical_cast< unsigned int >(std::string(begin, end));
			}

			void setYear(const char * begin, const char * end) const
			{
				date_.year_ = boost::lexical_cast< unsigned int >(std::string(begin, end));
			}

			void setHour(const char * begin, const char * end) const
			{
				date_.hour_ = boost::lexical_cast< unsigned int >(std::string(begin, end));
			}

			void setMinute(const char * begin, const char * end) const
			{
				date_.minute_ = boost::lexical_cast< unsigned int >(std::string(begin, end));
			}

			void setSecond(const char * begin, const char * end) const
			{
				date_.second_ = boost::lexical_cast< unsigned int >(std::string(begin, end));
			}

			void setTimeZone(const char * begin, const char * end) const
			{
				if (std::distance(begin, end) == 1)
				{
					static const std::string chars__("MLKIHGFEDCBAZNOPQRSTUVWXY");
					assert(chars__.find(*begin) != std::string::npos);
					date_.timezone_ = (static_cast< int >(chars__.find(*begin)) - 12) * 60;
				}
				else
				{
					assert(std::distance(begin, end) == 4);
					unsigned int hours(boost::lexical_cast< unsigned int >(std::string(begin, begin + 2)));
					unsigned int minutes(boost::lexical_cast< unsigned int >(std::string(begin + 2, end)));
					date_.timezone_ = minutes + (60 * hours);
				}
			}

			template < typename ScannerT >
			struct definition
			{
				definition(const Grammar & self)
				{
					using namespace boost::spirit;

					day_of_week_	= str_p("Mon")[SetDayOfWeek(self.date_, 1)]
									| str_p("Tue")[SetDayOfWeek(self.date_, 2)]
									| str_p("Wed")[SetDayOfWeek(self.date_, 3)]
									| str_p("Thu")[SetDayOfWeek(self.date_, 4)]
									| str_p("Fri")[SetDayOfWeek(self.date_, 5)]
									| str_p("Sat")[SetDayOfWeek(self.date_, 6)]
									| str_p("Sun")[SetDayOfWeek(self.date_, 0)]
									;
					month_			= str_p("Jan")[SetMonth(self.date_,  1)]
									| str_p("Feb")[SetMonth(self.date_,  2)]
									| str_p("Mar")[SetMonth(self.date_,  3)]
									| str_p("Apr")[SetMonth(self.date_,  4)]
									| str_p("May")[SetMonth(self.date_,  5)]
									| str_p("Jun")[SetMonth(self.date_,  6)]
									| str_p("Jul")[SetMonth(self.date_,  7)]
									| str_p("Aug")[SetMonth(self.date_,  8)]
									| str_p("Sep")[SetMonth(self.date_,  9)]
									| str_p("Oct")[SetMonth(self.date_, 10)]
									| str_p("Nov")[SetMonth(self.date_, 11)]
									| str_p("Dec")[SetMonth(self.date_, 12)]
									;
					day_			= lexeme_d[ !digit_p >> digit_p ]
									;
					year_			= lexeme_d[ digit_p >> digit_p >> !( digit_p >> digit_p ) ]
									;
					date_			= day_[boost::bind(&Grammar::setDay, &self, _1, _2)] >> month_ >> year_[boost::bind(&Grammar::setYear, &self, _1, _2)]
									;
					hour_min_sec_	= lexeme_d[ !digit_p >> digit_p ]	// we are slightly more permissive than the standard in that we also parse minutes and seconds on only one digit
									;
					wall_time_		= hour_min_sec_[boost::bind(&Grammar::setHour, &self, _1, _2)] >> ':' >> hour_min_sec_[boost::bind(&Grammar::setMinute, &self, _1, _2)] >> !( ':' >> hour_min_sec_[boost::bind(&Grammar::setSecond, &self, _1, _2)])
									;
					four_digit_tz_	= lexeme_d[ digit_p >> digit_p >> digit_p >> digit_p ]
									;
					single_char_tz_	= chset_p("A-IK-Z")
									;
					zone_			= str_p("UT")[SetTimeZone(self.date_, 0, 0)]
									| str_p("UTC")[SetTimeZone(self.date_, 0, 0)]
									| str_p("GMT")[SetTimeZone(self.date_, 0, 0)]
									| str_p("EST")[SetTimeZone(self.date_, -5, 0)]
									| str_p("EDT")[SetTimeZone(self.date_, -4, 0)]
									| str_p("CST")[SetTimeZone(self.date_, -6, 0)]
									| str_p("CDT")[SetTimeZone(self.date_, -5, 0)]
									| str_p("MST")[SetTimeZone(self.date_, -7, 0)]
									| str_p("MDT")[SetTimeZone(self.date_, -6, 0)]
									| str_p("PST")[SetTimeZone(self.date_, -8, 0)]
									| str_p("PDT")[SetTimeZone(self.date_, -7, 0)]
									| single_char_tz_[boost::bind(&Grammar::setTimeZone, &self, _1, _2)]
									| four_digit_tz_[boost::bind(&Grammar::setTimeZone, &self, _1, _2)]
									;
					time_			= wall_time_ >> zone_
									;
					rule_			= !( day_of_week_ >> ',' ) >> date_ >> time_
									;
				}

				boost::spirit::rule< ScannerT > start() const
				{
					return rule_;
				}

				boost::spirit::rule< ScannerT > day_of_week_;
				boost::spirit::rule< ScannerT > day_;
				boost::spirit::rule< ScannerT > month_;
				boost::spirit::rule< ScannerT > year_;
				boost::spirit::rule< ScannerT > zone_;
				boost::spirit::rule< ScannerT > wall_time_;
				boost::spirit::rule< ScannerT > hour_min_sec_;
				boost::spirit::rule< ScannerT > date_;
				boost::spirit::rule< ScannerT > four_digit_tz_;
				boost::spirit::rule< ScannerT > single_char_tz_;
				boost::spirit::rule< ScannerT > time_;
				boost::spirit::rule< ScannerT > rule_;
			};

			Date & date_;
		};

		DateParser::Date DateParser::parse(const std::string & input)
		{
			Date retval;
			Grammar grammar(retval);

			if (!boost::spirit::parse(input.c_str(), grammar, boost::spirit::space_p).full || !validate(retval))
			{	// parse error
				throw std::runtime_error("Invalid date");
			}
			else
			{ /* all is well */ }

			return retval;
		}

		bool DateParser::validate(const Date & date)
		{
			struct ValidationHelper
			{
				static bool isLeapYear(unsigned int year)
				{
					return (((year % 4) == 0) && (!(year % 100 == 0) || (year % 400 == 0)));
				}

				static Date::Day getDayOfWeek(unsigned int day, unsigned int month, unsigned int year)
				{
					static const unsigned int centuries__[] = { 4, 2, 0, 6, 4 };
					static const unsigned int months__[] = { 0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5 };
					static const unsigned int months_leap__[] = { 6, 2, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5 };

					unsigned int score(day);
					// lookup century
					assert(year >= 1700 && year <= 2199);
					score += centuries__[(year / 100) - 17];
					// lookup year
					score += (year % 100);
					score += (year % 100) / 4;
					// lookup month
					assert(month >= 1 && month <= 12);
					if (isLeapYear(year))
						score += months_leap__[month - 1];
					else
						score += months_leap__[month - 1];
					return static_cast< Date::Day >(score % 7);
				}
			};
			static const unsigned int month_days__[] = {
				0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
			};

			if (date.day_of_week_ < Date::sunday__ || date.day_of_week_ > Date::saturday__)
				return false;
			if (date.month_ < Date::january__ || date.month_ > Date::december__)
				return false;
			if (date.month_ == 2 && ValidationHelper::isLeapYear(date.year_) && (date.day_ > (month_days__[2] + 1)))
				return false;
			if (date.day_ > month_days__[date.month_])
				return false;
			if (ValidationHelper::getDayOfWeek(date.day_, date.month_, date.year_) != date.day_of_week_)
				return false;
			if (date.hour_ >= 24 || date.minute_ >= 60 || date.second_ >= 60)
				return false;
			if (date.timezone_ < -720 || date.timezone_ > 720)
				return false;
			return true;
		}
	}
}
