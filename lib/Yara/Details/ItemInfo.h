/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_iteminfo_h
#define _yara_details_iteminfo_h
/** \file Yara/Details/ItemInfo.h Defines the ItemInfo structure. */

namespace Yara
{
	namespace Details
	{
		//! This structure contains the necessary information about an item in an RSS stream
		struct ItemInfo
		{
			ItemInfo()
			{ /* no-op */ }

			ItemInfo(const std::string & title, const std::string & link, const std::string & description)
				: title_(title),
				  link_(link),
				  description_(description)
			{ /* no-op */ }

			//! Title of the item
			std::string title_;
			//! Link to the item
			std::string link_;
			//! Description of the item
			std::string description_;
		};
	}
}

#endif
