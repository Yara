/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_imageinfo_h
#define _yara_details_imageinfo_h
/** \file Yara/Details/ImageInfo.h Defines the ImageInfo struct which regroups all the information about an image. */

namespace Yara
{
	namespace Details
	{
		//! All the necessary information about an image
		struct ImageInfo
		{
			//! Title of the image
			std::string title_;
			//! URL of the image
			std::string url_;
			//! Link with the image
			std::string link_;
			//! Width of the image
			unsigned int width_;
			//! Height of the image
			unsigned int height_;
			//! Description of the image
			std::string description_;
		};
	}
}

#endif
