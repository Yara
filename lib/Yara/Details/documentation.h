/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_documentation_h
#define _yara_details_documentation_h
/** \file Yara/Details/documentation.h Internal use only - for Doxygen documentation */

//! Library namespace. Everything in the C++ API is in this namespace
namespace Yara
{
	//! Namespace in which all exceptions thrown by this library are defined
	namespace Exceptions
	{ /* here for documentation only */ }
	//! Namespace with things your compiler needs to know about, but you don't.
	namespace Details
	{ /* here for documentation only */ }
}

/** \mainpage Yara: Yet Another RSS Aggregator
 * Yara proposes two APIs: a C API and a C++ API. The C API is intended and 
 * designed to be used as an easy plug-in into applications that want to 
 * aggregate RSS streams to render them, while the C++ API provides a more 
 * fine-grained control over how parsing is done, and provides access to 
 * the behind-the-scenes utilities the parser uses. Yara itself is written 
 * in C++.
 *
 * Every part of Yara is designed to be both fast and extensible. The C API
 * consists of a few functions that comprise the core of the library's 
 * functionalities, to which acess to other parts of the library can be added
 * if need be. This API is intended to be very stable, and therefore has a
 * no-frills approach.
 *
 * The main C++ API consists of three classes: \link Yara::Template Template
 * \endlink, \link Yara::Parser Parser \endlink and \link Yara::Renderer Renderer 
 * \endlink.
 * The \link Yara::Template Template \endlink class provides a way to handle 
 * templates and makes it possible for Yara to generate things other than just
 * HTML (as most RSS renderers do) but also (for example) C code. The \link
 * Yara::Parser Parser \endlink class extracts information from the RSS 
 * stream and makes it accessible to the \link Yara::Renderer Renderer \endlink,
 * which uses an instance of \link Yara::Template Template \endlink to render
 * what the \link Yara::Parser Parser \endlink found. */

#endif
