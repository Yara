/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_dateparser_h
#define _yara_details_dateparser_h
/** \file Yara/Details/DateParser.h Defines a class that can parse a date in any of the formats allowed by RSS. */

#include "prologue.h"
#include <string>

namespace Yara
{
	namespace Details
	{
		/** \brief Parses the date in any of the formats allowed by RSS. */
		class Yara_API DateParser
		{
		public :
			//! The date, as returned by DateParser::parse
			struct Date
			{
				//! Default constructor - initializes only the second, as the rest should be found in the date to parse
				Date()
					: second_(0)
				{}

				//! An enumeration value for every day in the week
				enum Day
				{
					sunday__,
					monday__,
					tuesday__,
					wednesday__,
					thursday__,
					friday__,
					saturday__
				};
				//! An enumeration value for every month in the year
				enum Month
				{
					january__ = 1,
					february__,
					march__,
					april__,
					may__,
					june__,
					july__,
					august__,
					september__,
					october__,
					november__,
					december__
				};
				
				//! The day of the week found in the date
				Day day_of_week_;
				//! The day found in the date
				unsigned int day_;
				//! The month found in the date
				Month month_;
				//! The year found in the date
				unsigned int year_;
				//! The hour found in the date
				unsigned int hour_;
				//! The minute found in the date
				unsigned int minute_;
				//! The second found in the date
				unsigned int second_;
				//! The time-zone found in the date, as an offset in minutes
				int timezone_;
			};

			//! Parse the given string into an instance of Date
			static Date parse(const std::string & input);

		private :
			struct Grammar;

			static bool validate(const Date & date);
		};
	}
}

#endif
