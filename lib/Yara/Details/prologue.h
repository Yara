/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_prologue_h
#define _yara_details_prologue_h
/** \file Yara/Details/prologue.h Included by all files in the C and C++ APIs to define the Yara_API macro */

#ifndef DOXYGEN_GENERATING
#	if defined( _WIN32 ) && ! defined ( __CYGWIN__ )
#		ifdef Yara_EXPORTS
#			define Yara_API __declspec(dllexport)
#		else
#			define Yara_API __declspec(dllimport)
#		endif
#	else
#		define Yara_API
#	endif
#endif

#endif
