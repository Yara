/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_elementtype_h
#define _yara_details_elementtype_h
/** \file Yara/Details/ElementType.h Defines the enumerator for all element types known to the RSS standards. */

namespace Yara
{
	namespace Details
	{
		//! This enumerator contains a unique value for each known type of element found in an RSS stream.
		enum ElementType
		{
			none__,
			/* root element */
			rss__,						// <rss>, has a mandatory attribute "version" which should be 0.91, 0.92 or 2.0 for this implementation
			/* mandatory, single element under <rss> */
			channel__,					// contains information about the channel (metadata) and its contents.
			/* required channel elements */
			channel_title__,			// The name of the channel. It's how people refer to your service. If you have an HTML 
										// website that contains the same information as your RSS file, the title of your channel 
										// should be the same as the title of your website.
			channel_link__,				// The URL to the HTML website corresponding to the channel.
			channel_description__,		// Phrase or sentence describing the channel.
			/* Optional channel elements */
			channel_language__,			// The language the channel is written in. This allows aggregators to group all Italian 
										// language sites, for example, on a single page. A list of allowable values for this 
										// element, as provided by Netscape, is in Details/ChannelLanguage.h. We do not support anything 
										// not in that list
			channel_copyright__,		// Copyright notice for content in the channel
			managing_editor__,			// Email address for person responsible for editorial content
			webmaster__,				// Email address for person responsible for technical issues relating to channel
			channel_pub_date__,			// The publication date for the content in the channel. For example, the New York Times 
										// publishes on a daily basis, the publication date flips once every 24 hours. That's when 
										// the pubDate of the channel changes. All date-times in RSS conform to the Date and Time 
										// Specification of RFC 822, with the exception that the year may be expressed with two characters
										// or four characters (four preferred).
			last_build_date__,			// The last time the content of the channel changed.
			channel_category__,			// Specify one or more categories that the channel belongs to. Follows the same rules as the 
										// <item>-level category element.
			generator__,				// A string indicating the program used to generate the channel.
			docs__,						// A URL that points to the documentation for the format used in the RSS file. It's for people who 
										// might stumble across an RSS file on a Web server 25 years from now and wonder what it is.
			cloud__,					// Allows processes to register with a cloud to be notified of updates to the channel, implementing 
										// a lightweight publish-subscribe protocol for RSS feeds.
			ttl__,						// ttl stands for time to live. It's a number of minutes that indicates how long a channel can be 
										// cached before refreshing from the source.
			channel_image__,			// Specifies a GIF, JPEG or PNG image that can be displayed with the channel.
			rating__,					// The PICS rating for the channel.
			text_input__,				// Specifies a text input box that can be displayed with the channel
			skip_hours__,				// A hint for aggregators telling them which hours they can skip.
			skip_days__,				// A hint for aggregators telling them which days they can skip
		/* <image> sub-element of <channel> */
			/* required elements */
			image_url__,				// the URL of a GIF, JPEG or PNG image that represents the channel.
			image_title__,				// describes the image, it's used in the ALT attribute of the HTML <img> tag when the channel
										// is rendered in HTML. 
			image_link__,				// is the URL of the site, when the channel is rendered, the image is a link to the site. 
										// (Note, in practice the image <title> and <link> should have the same value as the channel's 
										// <title> and <link>.
			/* optional elements */
			image_width__,				// width of the image in pixels - 144 max, 88 default 
			image_height__,				// height of the image in pixels - 400 max, 31 default
			image_description__,		// contains text that is included in the TITLE attribute of the link formed around the image in the
										// HTML rendering.
		/* <textInput> sub-element of <channel> */
			text_input_title__,			// The label of the Submit button in the text input area.
			text_input_description__,	// Explains the text input area.
			text_input_name__,			// The name of the text object in the text input area.
			text_input_link__,			// The URL of the CGI script that processes text input requests.
		/* <item> and its elements */
			item__,						// The <item> element. A channel may contain any number of <item>s. An item may represent a 
										// "story" -- much like a story in a newspaper or magazine; if so its description is a synopsis 
										// of the story, and the link points to the full story. An item may also be complete in itself, 
										// if so, the description contains the text (entity-encoded HTML is allowed; see examples), and 
										// the link and title may be omitted. All elements of an item are optional, however at least one 
										// of title or description must be present.
			item_title__,
			item_link__,
			item_description__,
			item_author__,
			item_category__,
			item_comments__,
			item_enclosure__,
			item_guid__,
			item_pub_date__,
			item_source__
		};
	}
}


#endif
