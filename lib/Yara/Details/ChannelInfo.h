/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_details_channelinfo_h
#define _yara_details_channelinfo_h
/** \file Yara/Details/ChannelInfo.h Defines the ChannelInfo struct which regroups all the information about a channel. */

#include <boost/shared_ptr.hpp>
#include "ImageInfo.h"
#include "Language.h"

namespace Yara
{
	namespace Details
	{
		//! Regroup all pertinent information about a channel
		struct ChannelInfo
		{
			//! Default constructor
			ChannelInfo()
				: language_(unknown__)
			{ /* no-op */ }

			//! Title of the channel
			std::string title_;
			//! Link to the channel site
			std::string link_;
			//! Description of the channel
			std::string description_;
			//! Language of the channel
			Details::Language language_;
			//! Copyright information of the channel
			std::string copyright_;
			//! Managing editor of the channel
			std::string managing_editor_;
			//! Webmaster of the channel
			std::string webmaster_;
			//! Image for the channel
			boost::shared_ptr< ImageInfo > image_;
		};
	}
}

#endif
