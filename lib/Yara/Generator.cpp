/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Generator.h"
#include <boost/lexical_cast.hpp>
#include "Exceptions/GeneratorExceptions.h"
#include "Details/ImageInfo.h"

namespace Yara
{
	std::string serialize(const Details::ImageInfo & image)
	{
		std::string retval = "\t\t<image>\n";
		retval += "\t\t\t<title>" + image.title_ + "</title>\n";
		retval += "\t\t\t<url>" + image.url_ + "</url>\n";
		retval += "\t\t\t<link>" + image.link_ + "</link>\n";
		retval += "\t\t\t<width>" + boost::lexical_cast< std::string >(image.width_) + "</width>\n";
		retval += "\t\t\t<height>" + boost::lexical_cast< std::string >(image.height_) + "</height>\n";
		retval += "\t\t\t<description>" + image.description_ + "</description>\n";
		retval += "\t\t</image>\n";

		return retval;
	}

	std::string serialize(const Details::ChannelInfo & channel_info)
	{
		std::string retval;

		retval += "\t\t<title>" + channel_info.title_ + "</title>\n";
		retval += "\t\t<link>" + channel_info.link_ + "</link>\n";
		retval += "\t\t<description>" + channel_info.description_ + "</description>\n";
		
		if (channel_info.language_ != Details::unknown__)
		{
			retval += "\t\t<language>" + getLanguage(channel_info.language_) + "</language>\n";
		}
		else
		{ /* no language field */ }
		if (!channel_info.copyright_.empty())
			retval += "\t\t<copyright>" + channel_info.copyright_ + "</copyright>\n";
		else
		{ /* no copyright element */ }
		if (!channel_info.managing_editor_.empty())
			retval += "\t\t<managingEditor>" + channel_info.managing_editor_ + "</managingEditor>\n";
		else
		{ /* no copyright element */ }
		if (!channel_info.webmaster_.empty())
			retval += "\t\t<webMaster>" + channel_info.webmaster_ + "</webMaster>\n";
		else
		{ /* no copyright element */ }
		
		if (channel_info.image_)
			retval += serialize(*channel_info.image_);
		else
		{ /* no image */ }

		return retval;
	}

	std::string serialize(const Details::ItemInfo & item)
	{
		std::string retval;

		retval += "\t\t<item>\n";
		retval += "\t\t\t<title>" + item.title_ + "</title>\n";
		retval += "\t\t\t<link>" + item.link_ + "</link>\n";
		retval += "\t\t\t<description>" + item.description_ + "</description>\n";
		retval += "\t\t</item>\n";

		return retval;
	}
	
	template < typename Container >
	std::string serialize(const Container & container)
	{
		std::string retval;
		for (typename Container::const_iterator where(container.begin()); where != container.end(); ++where)
			retval += serialize(*where);

		return retval;
	}

	Generator::Generator()
	{ /* no-op */ }

	Generator::~Generator()
	{ /* no-op */ }

	Generator & Generator::generate()
	{
		// an RSS feed should at least have a title, a link and a description in its channel info. If it doesn't, it is not valid
		if (channel_info_.title_.empty() || channel_info_.link_.empty() || channel_info_.description_.empty())
			throw Exceptions::ChannelInfoInvalid();
		else
		{ /* all is well */ }

		serialized_feed_ = 
			"<?xml version=\"1.0\" ?>\n"
			"<rss version=\"2.0\">\n"
			"\t<channel>\n";
		serialized_feed_ += serialize(channel_info_);
		serialized_feed_ += serialize(items_);
		serialized_feed_ += 
			"\t</channel>\n"
			"</rss>\n";

		return *this;
	}

	Generator::operator const char * () const
	{
		return serialized_feed_.c_str();
	}

	void Generator::reset()
	{
		Details::ChannelInfo temp;
		std::swap(channel_info_, temp);
		items_.clear();
		serialized_feed_.clear();
	}
}
