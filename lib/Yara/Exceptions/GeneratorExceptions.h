/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_exceptions_generatorexceptions_h
#define _yara_exceptions_generatorexceptions_h
/** \file Yara/Exceptions/GeneratorExceptions.h Defines the exceptions thrown by the generator
 * This file is part of the C++ API for Yara. It defines all of the 
 * exception classes for the generator, each of which directly or indirectly 
 * derives from std::exception. Exceptions that indicate bugs in either the 
 * client app or Yara, are derived from std::logic_error. All others are derived 
 * from std::runtime_error. */
#ifdef DOXYGEN_GENERATING
#include <Yara/Exceptions/ParserExceptions.h>
#else
#include <stdexcept>
#endif

namespace Yara
{
	namespace Exceptions
	{
		//! An instance of this class is thrown when the parser reports it's not done parsing, when it should be.
		struct ChannelInfoInvalid : std::runtime_error
		{
			ChannelInfoInvalid()
				: std::runtime_error("Invalid or empty channel info")
			{}
		};
	}
}

#endif
