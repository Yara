/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_exceptions_parserexceptions_h
#define _yara_exceptions_parserexceptions_h
/** \file Yara/Exceptions/ParserExceptions.h Defines the exceptions thrown by the parser, template and renderer of the C++ API
 * This file is part of the C++ API for Yara. It defines all of the 
 * exception classes, each of which directly or indirectly derives from 
 * std::exception. Exceptions that indicate bugs in either th client app 
 * or Yara, are derived from std::logic_error. All others are derived 
 * from std::runtime_error. */
#ifdef DOXYGEN_GENERATING
#include <Yara/Exceptions/ParserExceptions.h>
#else
#include <stdexcept>
#endif

namespace Yara
{
	namespace Exceptions
	{
		//! An instance of this class is thrown when the unthinkable happens
		struct InternalError : std::logic_error
		{
			enum Reason
			{
				//! The XML parser was somehow suspended. Nothing know can cause this
				xml_parser_suspended__,
				//! The XML parser (Expat) returned an unknown value. Probably a bug in Expat, or an incompatible version.
				xml_parser_returned_unknown_value__,
				//! Unreachable code was reached. A bug in the compiler?
				the_unreachable_was_reached__
			};

			InternalError(Reason reason)
				: std::logic_error("internal error (BUG)"),
				  reason_(reason)
			{ /* no-op */ }

			Reason reason_;
		};

		//! An instance of this class is thrown when the parser reports it's not ready while it should be.
		struct ParserNotReady : std::logic_error
		{
			ParserNotReady()
				: std::logic_error("Parser not ready")
			{}
		};

		//! An instance of this class is thrown when the parser reports it's not done parsing, when it should be.
		struct ParserNotDone : std::logic_error
		{
			ParserNotDone()
				: std::logic_error("Parser not done")
			{}
		};

		//! An instance of this class is thrown when there's an error in the XML of the RSS stream
		struct XMLError : std::runtime_error
		{
			XMLError(int line, int col, int byte)
				: std::runtime_error("XML error"),
				  line_(line),
				  col_(col),
				  byte_(byte)
			{}

			int line_;
			int col_;
			int byte_;
		};

		//! An instance of this class is thrown when the RSS stream is of a version that we cannot parse.
		struct RSSVersionError : std::runtime_error
		{
			enum { max_version_string_size__ = 5 };
			RSSVersionError(const std::string & found_version)
				: std::runtime_error("wrong RSS stream version")
			{
				const unsigned int len(found_version.size() > max_version_string_size__ ? max_version_string_size__ : found_version.size());
				unsigned int offs(0);
				while (offs < len)
				{
					found_version_[offs] = found_version[offs];
					++offs;
				}
				found_version_[offs] = 0;
			}

			char found_version_[max_version_string_size__ + 1];
		};

		//! An instance of this class is thrown when the RSS stream contains something it shouldn't
		struct RSSContentError : std::runtime_error
		{
			enum What {
				unexpected_markup__,
			};
	
			RSSContentError(What what)
				: std::runtime_error("Error in the stream content"),
				  what_(what)
			{ /* no-op */ }

			What what_;
		};

		//! An instance of this class is thrown when the parser encounters a language tag it doesn't know.
		struct UnknownLanguage : std::runtime_error
		{
			UnknownLanguage(const std::string & language_code)
				: std::runtime_error("Unknown language")
			{
				if (language_code.size() > 5)
				{
					std::copy(language_code.begin(), language_code.begin() + 5, code_);
					code_[5] = 0;
				}
				else
				{
					std::copy(language_code.begin(), language_code.end(), code_);
					code_[language_code.size()] = 0;
				}
			}

			char code_[6];
		};
	}
}

#endif
