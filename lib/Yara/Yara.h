/*
 * Yara: Yet Another RSS Aggregator
 * Copyright (C) 2007  Ronald Landheer-Cieslak
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _yara_yara_h
#define _yara_yara_h
/** \file Yara/Yara.h C API for Yara.
 * This file defines the C API for Yara, for use by programs either not written 
 * in C++, or compiled with a different compiler, or using Yara as a plug-in. */
#ifdef DOXYGEN_GENERATING
#include <Yara/Yara.h>
#else
#include "Details/prologue.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct YaraItemInfo_ YaraItemInfo;
typedef struct YaraImageInfo_ YaraImageInfo;
typedef struct YaraChannelInfo_ YaraChannelInfo;
typedef struct YaraGenerator_ YaraGenerator;

/** \brief Render an Yara stream into the target using the provided template.
 * This function uses the provided template file to render an Yara stream
 * into the given target file.
 * \returns 0 on success, nonzero on failure.
 * \param target_filename name of the file to render into
 * \param template_filename compiled template to use
 * \param rss_xml Yara stream to parse and render */
Yara_API int Yara_render(const char * target_filename, const char * template_filename, const char * rss_xml);
/** \brief Compile a Yara template from three files.
 * A template consists of a header template, a footer template, and the template
 * for each item. Yara will replace any tag it finds in the template with the 
 * proper content, as found in the RSS stream. This function allows you to compile
 * a template so Yara can use it.
 * \param target_filename name of the file to store the template in
 * \param header_filename name of the file that contains the header template
 * \param footer_filename name of the file that contains the footer template
 * \param item_filename name of the file that contains the item template
 * \returns 0 on success, non-zero on failure */
Yara_API int Yara_compileTemplate(const char * target_filename, const char * header_filename, const char * footer_filename, const char * item_filename);
/** \brief Get the most recent error message.
 * As Yara is written in C++ but this API is a C API, exceptions will not
 * be propagated by this API. Rather, the \c what message of any exception
 * caught is stored in a stack, which this function and Yara_popLastError give
 * access to. This function will return the message on the top of the stack, or
 * 0 if there is none.
 * \returns: a pointer to a contiguous string of characters, ending in a 0
 *           and representing a human-readable text indicating the most 
 *           recent error on the stack. */
Yara_API const char * Yara_getLastError();
/** \brief pop the most recent error message off the top of the message stack.
 * See Yara_getLastError for more information. */
Yara_API void Yara_popLastError();

Yara_API YaraItemInfo * YaraItemInfo_new();
Yara_API void YaraItemInfo_delete(YaraItemInfo * channel_info);
Yara_API int YaraItemInfo_setTitle(YaraItemInfo * channel_info, const char * title);
Yara_API const char * YaraItemInfo_getTitle(YaraItemInfo * channel_info);
Yara_API int YaraItemInfo_setLink(YaraItemInfo * channel_info, const char * link);
Yara_API const char * YaraItemInfo_getLink(YaraItemInfo * channel_info);
Yara_API int YaraItemInfo_setDescription(YaraItemInfo * channel_info, const char * desxription);
Yara_API const char * YaraItemInfo_getDescription(YaraItemInfo * channel_info);

Yara_API YaraImageInfo * YaraImageInfo_new();
Yara_API void YaraImageInfo_delete(YaraImageInfo * channel_info);
Yara_API int YaraImageInfo_setTitle(YaraImageInfo * channel_info, const char * title);
Yara_API const char * YaraImageInfo_getTitle(YaraImageInfo * channel_info);
Yara_API int YaraImageInfo_setURL(YaraImageInfo * channel_info, const char * url);
Yara_API const char * YaraImageInfo_getURL(YaraImageInfo * channel_info);
Yara_API int YaraImageInfo_setLink(YaraImageInfo * channel_info, const char * link);
Yara_API const char * YaraImageInfo_getLink(YaraImageInfo * channel_info);
Yara_API int YaraImageInfo_setDescription(YaraImageInfo * channel_info, const char * description);
Yara_API const char * YaraImageInfo_getDescription(YaraImageInfo * channel_info);
Yara_API int YaraImageInfo_setWidth(YaraImageInfo * channel_info, unsigned int width);
Yara_API unsigned int YaraImageInfo_getWidth(YaraImageInfo * channel_info);
Yara_API int YaraImageInfo_setHeight(YaraImageInfo * channel_info, unsigned int height);
Yara_API unsigned int YaraImageInfo_getHeight(YaraImageInfo * channel_info);

Yara_API YaraChannelInfo * YaraChannelInfo_new();
Yara_API void YaraChannelInfo_delete(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setTitle(YaraChannelInfo * channel_info, const char * title);
Yara_API const char * YaraChannelInfo_getTitle(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setLink(YaraChannelInfo * channel_info, const char * link);
Yara_API const char * YaraChannelInfo_getLink(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setDescription(YaraChannelInfo * channel_info, const char * desxription);
Yara_API const char * YaraChannelInfo_getDescription(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setLanguage(YaraChannelInfo * channel_info, const char * language);
Yara_API const char * YaraChannelInfo_getLanguage(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setCopyright(YaraChannelInfo * channel_info, const char * copyright);
Yara_API const char * YaraChannelInfo_getCopyright(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setManagingEditor(YaraChannelInfo * channel_info, const char * managing_editor);
Yara_API const char * YaraChannelInfo_getManagingEditor(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setWebmaster(YaraChannelInfo * channel_info, const char * webmaster);
Yara_API const char * YaraChannelInfo_getWebmaster(YaraChannelInfo * channel_info);
Yara_API int YaraChannelInfo_setImage(YaraChannelInfo * channel_info, YaraImageInfo * image);
Yara_API YaraImageInfo * YaraChannelInfo_getImage(YaraChannelInfo * channel_info);

Yara_API YaraGenerator * YaraGenerator_new();
Yara_API void YaraGenerator_delete(YaraGenerator * generator);
Yara_API int YaraGenerator_setChannelInfo(YaraGenerator * generator, YaraChannelInfo * channel_info);
Yara_API void YaraGenerator_reset(YaraGenerator * generator);
Yara_API int YaraGenerator_addItem(YaraGenerator * generator, YaraItemInfo * item);
Yara_API const char * YaraGenerator_generate(YaraGenerator * generator);

#ifdef __cplusplus
}
#endif

#endif
